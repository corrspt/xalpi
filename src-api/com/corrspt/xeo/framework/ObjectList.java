package com.corrspt.xeo.framework;

import netgest.bo.data.DataResultSet;
import netgest.bo.def.boDefHandler;
import netgest.bo.ql.QLParser;
import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boObject;
import netgest.bo.runtime.boObjectList.SqlField;
import netgest.bo.runtime.boRuntimeException;

/**
 * Represents a list of objects and associated properties and metadata
 *
 */
public interface ObjectList {

	public byte getFormat();

	public boDefHandler getBoDef() throws boRuntimeException;

	public String getName();

	public long getCurrentBoui();

	/**
	 * Whether or not the list contains a given identifier
	 * 
	 * @param boui The identifier
	 * 
	 * @return True if the identifier is in the list and false otherwise
	 */
	public boolean haveBoui( long boui );

	public String getBOQL();

	public Object[] getBOQLArgs();

	public boolean haveMorePages();

	public void refreshData();

	/**
	 * Checks whether the list empty or not
	 * 
	 * @return
	 */
	public boolean isEmpty();

	public String getFilter();

	public boObject getObject( long boui ) throws boRuntimeException;

	public boObject getObject() throws boRuntimeException;

	/**
	 * 
	 * Returns the number of rows in the current page
	 * 
	 * @return The number of rows in the current page
	 */
	public int getRowCount();

	/**
	 * 
	 * Retrieves the number of records returned by the select query
	 * 
	 * @return The total number of records in the query
	 */
	public long getRecordCount();

	
	public int getPages();

	public int getPageSize();

	public int getPage();

	public boolean next();

	public boolean moveTo( int recno );

	public void nextPage();

	public void previousPage();

	public void firstPage();

	public void lastPage();

	public boolean previous();

	public void beforeFirst();
	
	public boolean first();

	public boolean last();

	public String getOrderBy();

	public String[] getParametersNames();

	public String[] getParametersValues();

	public String getParameter( String parametername );

	public void removeCurrent() throws boRuntimeException;

	public void moveRowTo( int row ) throws boRuntimeException;

	public String getFullTextSearch();

	public QLParser getQLParser();

	public void setSqlFields( SqlField[] object );

	public SqlField[] getSqlFields();
	
	public EboContext getEboContext();

	public void setEboContext( EboContext oEboContext );

	public void setQueryOrderBy( String string );

	public void setUserQuery(String userQuery, Object[] userQueryParam);

	public DataResultSet getRslt();

	public void setFullTextSearch( String arrangeFulltext );
	
	public int getRow();

	public String getUserQuery();

	public Object[] getUserQueryArgs();


}