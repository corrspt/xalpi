package com.corrspt.xeo.framework;

import netgest.bo.def.boDefHandler;

public interface ModelMetadataProvider extends ServiceProvider {

	/**
	 * 
	 * Retrieves the Model Definition for a given model
	 * 
	 * @param name The model name
	 * @return A wrapper around the Model Definition
	 */
	public boDefHandler getBoDefinition(String name);
	
	
}
