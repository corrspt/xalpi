package com.corrspt.xeo.framework;

import netgest.bo.lovmanager.lovObject;
import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boRuntimeException;

public interface LovProvider extends ServiceProvider {

	public lovObject getLovObject(EboContext ctx, long lovBoui) throws boRuntimeException ;
	
	public lovObject getLovObject(EboContext ctx, String lovName) throws boRuntimeException ;
	
	
}
