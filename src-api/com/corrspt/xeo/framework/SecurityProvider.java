package com.corrspt.xeo.framework;

import netgest.bo.runtime.boObject;
import netgest.bo.runtime.boRuntimeException;

public interface SecurityProvider extends ServiceProvider {

	public boolean canRead(boObject o) throws boRuntimeException;
    
    public boolean canDelete(boObject o) throws boRuntimeException;

    public boolean canWrite(boObject o) throws boRuntimeException;

    public boolean hasFullControl(boObject o) throws boRuntimeException;	
}
