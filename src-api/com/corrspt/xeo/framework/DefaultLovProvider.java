package com.corrspt.xeo.framework;

import netgest.bo.lovmanager.LovManager;
import netgest.bo.lovmanager.lovObject;
import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boRuntimeException;

public class DefaultLovProvider implements LovProvider {

	@Override
	public lovObject getLovObject( EboContext ctx, long lovBoui ) throws boRuntimeException {
		return LovManager.getLovObject( ctx, lovBoui );
	}

	@Override
	public lovObject getLovObject( EboContext ctx, String lovName ) throws boRuntimeException {
		return LovManager.getLovObject( ctx, lovName );
	}

}
