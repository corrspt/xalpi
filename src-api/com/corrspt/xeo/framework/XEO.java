package com.corrspt.xeo.framework;

import java.util.HashMap;
import java.util.Map;


/**
 * 
 * A Service Locator (with service registering) for the various Services
 * provided by the framework (object loading, security definition, metadata)
 * 
 * Implemented as a singleton
 * 
 * @author PedroRio
 *
 */
public class XEO {
	
	/**
	 * Protected constructor to the singleton
	 */
	protected XEO(){
		registerDefaultServiceProviders();
	}

	/**
	 * Register the default provider
	 */
	private void registerDefaultServiceProviders() {
		this.registerService( new DefaultSecurityProvider(), Services.SECURITY );
		this.registerService( new DefaultMetadataProvider(), Services.METADATA );
		this.registerService( new DefaultObjectProvider(), Services.OBJECT );
		this.registerService( new DefaultObjectListProvider(), Services.OBJECT_LIST );
	}

	public enum Services{
		OBJECT,
		OBJECT_LIST,
		SECURITY,
		METADATA
	}
	
   protected static XEO soleInstance = null;
	  
   public static XEO get() {
	   if ( soleInstance == null )
		   synchronized( XEO.class ) {
			   if ( soleInstance == null )
				   soleInstance = new XEO();
		   }
	   return soleInstance;
   }
        
   private Map<String,ServiceProvider> providers = new HashMap<String,ServiceProvider>();

   public ServiceProvider findService(String serviceName) {
      return providers.get(serviceName);
   }
   
   public ServiceProvider findService(Services serviceName) {
      return providers.get(serviceName.name());
	   }
  
   
   public void registerServiceForName( ServiceProvider provider, String serviceName) {
      providers.put( serviceName, provider);
   }
   
   public void registerService( ServiceProvider provider, Services serviceName) {
	      providers.put( serviceName.name(), provider);
   }
   
   
	//Getters for Default XEO Providers
	/**
	 * 
	 * Returns the Security Provider
	 * 
	 * @return An instance of the security provider
	 */
	public static SecurityProvider getSecurityProvider(){
		return (SecurityProvider) get().findService( Services.SECURITY );
	}
	
	/**
	 * 
	 * Returns the Metadata Provider
	 * 
	 * @return An instance of the metadata provider
	 */
	public static ModelMetadataProvider getMetadataProvider(){
		return (ModelMetadataProvider) get().findService( Services.METADATA );
	}
	
	/**
	 * 
	 * Returns the Object Provider
	 * 
	 * @return An instance of the object provider
	 */
	public static ObjectProvider getObjectProvider(){
		return (ObjectProvider) get().findService( Services.OBJECT );
	}
	
	/**
	 * Returns the Object List Provider
	 * 
	 * @return An instance of the object list provider
	 */
	public static ObjectListProvider getListProvider(){
		return (ObjectListProvider) get().findService( Services.OBJECT_LIST );
	}
	
	
}
