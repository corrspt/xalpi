package com.corrspt.xeo.framework;

import netgest.bo.runtime.boObject;
import netgest.bo.runtime.boRuntimeException;
import netgest.bo.security.securityOPL;

public class DefaultSecurityProvider implements SecurityProvider {

	@Override
	public boolean canRead( boObject o ) throws boRuntimeException {
		return securityOPL.canRead( o );
	}

	@Override
	public boolean canDelete( boObject o ) throws boRuntimeException {
		return securityOPL.canDelete( o );
	}

	@Override
	public boolean canWrite( boObject o ) throws boRuntimeException {
		return securityOPL.canWrite( o );
	}

	@Override
	public boolean hasFullControl( boObject o ) throws boRuntimeException {
		return securityOPL.hasFullControl( o );
	}

}
