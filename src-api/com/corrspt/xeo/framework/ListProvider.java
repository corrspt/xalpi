package com.corrspt.xeo.framework;

import netgest.bo.runtime.boObject;

public interface ListProvider extends ServiceProvider {

	public boObject getObject();
	
	public boolean next();
	
	public void beforeFirst();

	public long getRecordCount();
	
	public boolean haveBoui(long boui);
}
