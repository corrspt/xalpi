package com.corrspt.xeo.framework;

import com.corrspt.xeo.api.core.XeoObjectList;

import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boObjectList;

/**
 * The default implementation for a List of Objects
 *
 */
public class DefaultObjectListProvider implements ObjectListProvider {

	private static final boolean useSecurity = true;
	
	@Override
	public ObjectList list( EboContext ctx, String boql ) {
		return new XeoObjectList(boObjectList.list( ctx, boql ));
	}

	@Override
	public ObjectList list( EboContext ctx, String boql, Object[] params ) {
		return new XeoObjectList(boObjectList.list( ctx, boql, params ));
	}

	@Override
	public ObjectList list(EboContext ctx, String boql, Object[] params,
			boolean cache) {
		return new XeoObjectList(boObjectList.list( ctx, boql, params, cache, useSecurity ));
	}

}
