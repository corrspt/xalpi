package com.corrspt.xeo.framework;

import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boObject;
import netgest.bo.runtime.boRuntimeException;

/**
 * 
 * Provider to Load and Create objects
 *
 */
public interface ObjectProvider extends ServiceProvider {

	
	/**
	 * 
	 * Creates an instance of a Model given its name
	 * 
	 * @param ctx The context in which to create the instance
	 * @param name The name of the model
	 * @return An instance of the model
	 * 
	 * @throws boRuntimeException If an error occurs and the instance cannot be created
	 */
	public boObject createObject(EboContext ctx, String name ) throws boRuntimeException;

	/**
	 * 
	 * Creates an instance of Model given its name and associate it
	 * with a given parent
	 * 
	 * @param ctx The context in which to create the instance
	 * @param name The name of the parent
	 * @param parentBoui The identifier of the parent instance
	 * 
	 * @return An instance of the model
	 * 
	 * @throws boRuntimeException
	 */
	public boObject createObjectWithParent(EboContext ctx, String name, long parentBoui ) throws boRuntimeException;
	
	/**
	 * 
	 * Loads an instance given its identifier
	 * 
	 * @param ctx The context to load the instance
	 * @param boui The identifier of the instance
	 * @return
	 * @throws boRuntimeException
	 */
	public boObject loadObject(EboContext ctx,long boui) throws boRuntimeException;
	
	public boObject loadObject(EboContext ctx,String boql) throws boRuntimeException;
	
	
	
}
