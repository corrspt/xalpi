package com.corrspt.xeo.framework;

import netgest.bo.def.boDefHandler;

public class DefaultMetadataProvider implements ModelMetadataProvider {

	@Override
	public boDefHandler getBoDefinition( String name ) {
		return boDefHandler.getBoDefinition( name );
	}

}
