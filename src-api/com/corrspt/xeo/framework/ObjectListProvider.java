package com.corrspt.xeo.framework;

import netgest.bo.runtime.EboContext;

/**
 * 
 * Represents a provider of Object Lists, each method returns a list of objects
 * 
 */
public interface ObjectListProvider extends ServiceProvider {

	public ObjectList list(EboContext ctx, String boql);
	
	public ObjectList list(EboContext ctx, String boql, Object[] params);
	
	public ObjectList list(EboContext ctx, String boql, Object[] params, boolean cache);
	
	
	
}
