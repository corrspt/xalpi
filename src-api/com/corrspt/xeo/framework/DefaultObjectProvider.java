package com.corrspt.xeo.framework;

import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boObject;
import netgest.bo.runtime.boRuntimeException;

public class DefaultObjectProvider implements ObjectProvider {

	@Override
	public boObject createObject( EboContext ctx, String name ) throws boRuntimeException {
		return boObject.getBoManager().createObject( ctx, name );
	}

	@Override
	public boObject createObjectWithParent( EboContext ctx, String name, long parentBoui ) throws boRuntimeException {
		return boObject.getBoManager().createObjectWithParent( ctx, name, parentBoui );
	}

	@Override
	public boObject loadObject( EboContext ctx, long boui ) throws boRuntimeException {
		return boObject.getBoManager().loadObject( ctx, boui );
	}

	@Override
	public boObject loadObject( EboContext ctx, String boql ) throws boRuntimeException {
		return boObject.getBoManager().loadObject( ctx, boql ); 
	}
	
	

}
