package com.corrspt.xeo.framework;

/**
 * 
 * Marker interface to register service providers
 * 
 * @author PedroRio
 *
 */
public interface ServiceProvider {

}