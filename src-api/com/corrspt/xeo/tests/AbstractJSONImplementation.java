package com.corrspt.xeo.tests;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boConvertUtils;
import netgest.bo.runtime.boObject;
import netgest.bo.runtime.boRuntimeException;
import netgest.io.iFile;
import netgest.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.corrspt.xeo.api.core.BOUI;
import com.corrspt.xeo.api.core.BaseModel;

public abstract class AbstractJSONImplementation implements BaseModel {

	protected static final String EXISTS = "sys_exists";
	protected static final String CHANGED = "sys_changed";
	protected static final String DISABLED = "sys_disabled";
	protected static final String CLASSNAME = "sys_classname";
	protected static final String SYS_DTCREATE = "sys_dtcreate";
	protected static final String SYS_UPDATE = "sys_dtupdate";
	protected static final String PARENT = "parent$";
	protected static final String OBJ_ERRORS = "sys_obj_errors";
	protected static final String OBJ_ATTRIBUTE_ERRORS = "sys_att_errors";
	protected static final String BOUI = "BOUI";
	
	
	
	protected JSONObject attributes = new JSONObject();
	
	protected Map<String,iFile> ifiles = new HashMap<String, iFile>();
	
	protected Map<String,BaseModel> relations = new HashMap<String, BaseModel>();
	
	protected Map<String,List<BaseModel>> bridges = new HashMap<String, List<BaseModel>>();
	
	protected Map<String,String> parameters = new HashMap<String, String>();
	
	protected BaseModel parent;
	
	protected JSONObject system = new JSONObject();
	
	private final DateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
	private final DateFormat formatDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");
	
	protected Date convertStringToDate(String date){
		try {
			Date result = formatDateTime.parse(date);
			return result;
		} catch (ParseException e){
			try {
				Date result = formatDate.parse(date);
				return result;
			} catch (ParseException e1){
				
			}
		} 
		return null;  
	}
	
	protected String convertDateToString(Date date){
		try {
			String result = formatDateTime.format(date);
			return result;
		} catch (IllegalArgumentException e){
			try {
				String result = formatDate.format(date);
				return result;
			} catch (IllegalArgumentException e1){
				
			}
		} 
		return null;
	}
	
	
	
	protected void put(String key, Object value){
		try {
			attributes.put(key, value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	protected boolean contains(String key){
		return system.has(key) || attributes.has(key);
	}
	
	protected void removeKey(String key){
		system.remove(key);
	}
	
	protected AbstractJSONImplementation(JSONObject definition){
		putSystem(CHANGED, false);
		putSystem(EXISTS, false);
		
		try {
			if (definition.has(CLASSNAME))
				putSystem(CLASSNAME, definition.get(CLASSNAME));
			
			if (definition.has(DISABLED))
				putSystem(DISABLED, definition.get(DISABLED));
			else
				putSystem(DISABLED, Boolean.FALSE);
			
			if (definition.has(CHANGED))
				putSystem(CHANGED, definition.get(CHANGED));
			
			if (definition.has(OBJ_ERRORS)){
				putSystem(OBJ_ERRORS, definition.get(OBJ_ERRORS));
			}
			
			if (definition.has(OBJ_ATTRIBUTE_ERRORS)){
				putSystem(OBJ_ATTRIBUTE_ERRORS, definition.get(OBJ_ATTRIBUTE_ERRORS));
			}
			
			if (definition.has(BOUI)){
				put(BOUI, definition.get(BOUI));
			}
			
			if (definition.has(SYS_DTCREATE)){
				put(SYS_DTCREATE, definition.get(SYS_DTCREATE));
			}
			
			if (definition.has(SYS_UPDATE)){
				put(SYS_UPDATE, definition.get(SYS_UPDATE));
			}
			
		} catch (JSONException e){
			e.printStackTrace();
		}
	}
	
	protected AbstractJSONImplementation(){
		putSystem(CHANGED, false);
		putSystem(EXISTS, false);
	}
	
	protected void markChanged(){
		putSystem(CHANGED, true);
	}
	
	protected void putSystem(String key, Object value){
		try {
			system.put(key, value);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	protected String get(String key){
		try {
			if (attributes.has(key))
				return attributes.getString(key);
		} catch (JSONException e) {
			//e.printStackTrace();
		}
		return null;
	}
	
	protected String getString(String key){
		return get(key);
	}
	
	protected Date getDate(String key){
		if (attributes.has(key)){
			try {
				return new Date(attributes.getLong(key));
			} catch (JSONException e){
				e.printStackTrace();
			}
		}
		return null;
	}
	
	protected Long getLong(String key){
		try {
			return attributes.getLong(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected Boolean getBoolean(String key){
		try {
			if (attributes.has(key))
				return attributes.getBoolean(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected boolean getBoolSys(String key){
		try {
			if (system.has(key))
				return system.getBoolean(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	protected long getLongSys(String key){
		try {
			if (system.has(key))
				return system.getLong(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	protected String getStringSys(String key){
		try {
			if (system.has(key))
				return system.getString(key);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	@Override
	public void update() throws boRuntimeException {
		putSystem(EXISTS, true);
	}

	@Override
	public void destroy() throws boRuntimeException { }

	@Override
	public boObject getOriginal() {
		return null;
	}

	@Override
	public String getCardId() {
		//Gerado pelo 
		return null;
	}

	@Override
	public String getCardIdIcon() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getXeoModelName() {
		return get(CLASSNAME);
	}

	@Override
	public void addErrorMessage(String errorMessage) {
		try {
			if (!system.has(OBJ_ERRORS)){
				JSONArray messages = new JSONArray();
				messages.put(errorMessage);
				system.put(OBJ_ERRORS, messages);
			} else {
				JSONArray array;
				array = (JSONArray) system.get(OBJ_ERRORS);
				array.put(errorMessage);
			
			} 
		} catch (JSONException e ){
			e.printStackTrace();
		}
	}

	@Override
	public boolean hasErrors() {
		return (contains(OBJ_ERRORS) || contains(OBJ_ATTRIBUTE_ERRORS));
		
	}

	@Override
	public boolean hasAttributeErrors() {
		return contains(OBJ_ATTRIBUTE_ERRORS);
	}

	@Override
	public boolean hasErrorsInObjectOnly() {
		return contains(OBJ_ERRORS);
	}

	
	@Override
	public boolean exists() {
		if (system.has(EXISTS)){
			return getBoolSys(EXISTS);
		} else {
			putSystem(EXISTS, false);
		}
		return false;
	}

	@Override
	public void clearErrors() {
		removeKey(OBJ_ATTRIBUTE_ERRORS);
		removeKey(OBJ_ERRORS);
	}

	@Override
	public void clearObjectErrors() {
		removeKey(OBJ_ERRORS);
	}

	@Override
	public void clearAttributeErrors() {
		removeKey(OBJ_ATTRIBUTE_ERRORS);
	}

	@Override
	public boolean isChanged() {
		return getBoolSys(CHANGED);
	}

	@Override
	public void disable() {
		markChanged();
		putSystem(DISABLED, true);
	}

	@Override
	public boolean isDisabled() {
		if (system.has(DISABLED)){
			return getBoolSys(DISABLED);
		}
		return false;
	}

	@Override
	public void enable() {
		markChanged();
		putSystem("disabled", false);
	}

	@Override
	public boolean isEnabled() {
		if (system.has(DISABLED)){
			return !getBoolSys(DISABLED);
		}
		return true;
	}

	@Override
	public void setParent(BaseModel parent) {
		markChanged();
		this.parent = parent;
	}

	@Override
	public EboContext getEboContext() {
		return null;
	}

	@Override
	public BOUI getBoui() {
		String boui = get(BOUI);
		return new JSONBOUI(boui);
	}

	@Override
	public Date getDateCreate() {
		try{
			String date = getStringSys(SYS_DTCREATE);
			if (StringUtils.isEmpty(date))
				return null;
			Date dateCreate = boConvertUtils.convertToDate(date, null);
			return dateCreate;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Date getDateLastUpdate() {
		try{
			String date = getStringSys(SYS_UPDATE);
			if (StringUtils.isEmpty(date))
				return null;
			
			Date dateCreate = boConvertUtils.convertToDate(date, null);
			return dateCreate;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public void setBoui(BOUI boui){
		putSystem(BOUI, boui.serialize());
	}
	
	public void setBoui(Long boui){
		putSystem(BOUI, String.valueOf(boui));
	}
	
	public void setBoui(String boui){
		putSystem(BOUI, boui);
	}
	
	public void setDateCreate(String date){
		putSystem(SYS_DTCREATE, date);
	}
	
	public void setDateUpdate(String date){
		putSystem(SYS_UPDATE, date);
	}

	@Override
	public boolean hasParent() {
		return parent != null;
	}

	@Override
	public BaseModel getParent() {
		return parent;
	}

	@Override
	public boolean isSame(BaseModel o) {
		return o.getBoui().equals(getBoui());
	}

	@Override
	public boolean isValid() {
		return true;
	}
	
	private static class JSONBOUI implements BOUI {

		private String boui;
		
		public JSONBOUI(String boui){
			this.boui = boui;
		}
		
		@Override
		public Object getValue() {
			return boui;
		}

		@Override
		public String serialize() {
			return boui;
		}

		@Override
		public boolean equals(com.corrspt.xeo.api.core.BOUI o) {
			return o.serialize().equals(boui);
		}
		
	}
	
	public void setCustomParameter(String name, String value){
		parameters.put(name, value);
	}
	
	public String getCustomParameter(String name){
		return parameters.get(name);
	}
	
	public void removeParent() {
		this.parent = null;
	}
	
	

}
