package com.corrspt.xeo.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import netgest.bo.runtime.EboContext;
import netgest.io.iFile;
import netgest.io.iFileException;
import netgest.io.iFileFilter;
import netgest.io.iFilePermissionDenied;
import netgest.io.iFilenameFilter;
import netgest.io.metadata.iMetadataItem;

public class FakeIFile implements iFile {
	
	private File file;
	
	public FakeIFile(File theFile){
		this.file = theFile;
	}

	@Override
	public boolean addChild(iFile arg0) throws iFileException {return false;}

	@Override
	public void addMetadata(iMetadataItem arg0) throws iFileException {}

	@Override
	public boolean canRead() {return true;}

	@Override
	public boolean canWrite() {return true;}

	@Override
	public boolean checkIn() throws iFilePermissionDenied {return false;}

	@Override
	public boolean checkOut() throws iFilePermissionDenied {return false;}

	@Override
	public boolean close() {return false;}

	@Override
	public boolean createNewFile() throws IOException, iFilePermissionDenied {return false;}

	@Override
	public boolean delete() throws iFilePermissionDenied {return false;}

	@Override
	public boolean exists() {
		return file.exists();
	}

	@Override
	public String getAbsolutePath() {
		return file.getAbsolutePath();
	}

	@Override
	public List<iMetadataItem> getAllMetadata() throws iFileException {return new ArrayList<iMetadataItem>();}

	@Override
	public String getAuthor() {return "";}

	@Override
	public String getCategory() {return "";}

	@Override
	public String getCheckOutUser() {return "";}

	@Override
	public iFile getCopy() {return null;}

	@Override
	public iMetadataItem getDefaultMetadata() {return null;}

	@Override
	public String getDescription() {return "";}

	@Override
	public String[] getFileToDeleteOnCommit() {return new String[0];}

	@Override
	public String[] getFileToDeleteOnRollback() {return new String[0];}

	@Override
	public String getId() {
		return file.getAbsolutePath();
	}

	@Override
	public InputStream getInputStream() throws iFilePermissionDenied {
		try {
			return (new FileInputStream(file));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public long getKey() {return 0;}

	@Override
	public iMetadataItem getMetadata(String arg0) throws iFileException {return null;}

	@Override
	public List<iMetadataItem> getMetadataByName(String arg0)throws iFileException {return new LinkedList<iMetadataItem>();}

	@Override
	public List<String> getMetadataTypes() {return new LinkedList<String>();}

	@Override
	public String getName() {
		return file.getName();
	}

	@Override
	public String getParent() {return null;}

	@Override
	public iFile getParentFile() {return null;}

	@Override
	public String getPath() {return file.getAbsolutePath();
	}

	@Override
	public String getURI() {return file.getAbsolutePath();
	}

	@Override
	public long getVersion() {return 0;}

	@Override
	public String getVersionUser() {return "";}

	@Override
	public boolean inTransaction() {return true;}

	@Override
	public boolean isCheckedIn() {return false;}

	@Override
	public boolean isCheckedOut() {return false;}

	@Override
	public boolean isDirectory() {return false;}

	@Override
	public boolean isFile() {return true;}

	@Override
	public boolean isVersioned() {return false;}

	@Override
	public long lastModified() {return 0;}

	@Override
	public long length() {
		return file.length();
	}

	@Override
	public String[] list() throws iFilePermissionDenied {return new String[0];}

	@Override
	public String[] list(iFilenameFilter arg0) throws iFilePermissionDenied {return new String[0];}

	@Override
	public iFile[] listFiles() throws iFilePermissionDenied {return new iFile[0];}

	@Override
	public iFile[] listFiles(iFileFilter arg0) throws iFilePermissionDenied {return new iFile[0];}

	@Override
	public boolean makeVersioned() throws iFilePermissionDenied {return false;}

	@Override
	public boolean mkdir() throws iFilePermissionDenied {return false;}

	@Override
	public boolean mkdirs() throws iFilePermissionDenied {return false;}

	@Override
	public void removeMetadata(String arg0) {}

	@Override
	public boolean renameTo(iFile arg0) throws iFilePermissionDenied {return false;}

	@Override
	public void rollback(EboContext arg0) {}

	@Override
	public boolean save(EboContext arg0) throws iFileException {return true;}

	@Override
	public void setAuthor(String arg0) {}

	@Override
	public void setBinaryStream(InputStream arg0) throws iFilePermissionDenied {}

	@Override
	public void setCategory(String arg0) {}

	@Override
	public void setCheckOutUser(String arg0) {}

	@Override
	public void setDescription(String arg0) {}

	@Override
	public boolean setReadOnly() throws iFilePermissionDenied {
		return false;
	}

	@Override
	public void setVersionUser(String arg0) {}

	@Override
	public void updateFile(iFile arg0) {}

}
