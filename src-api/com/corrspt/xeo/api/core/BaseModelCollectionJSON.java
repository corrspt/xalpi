package com.corrspt.xeo.api.core;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
*
* Base Model
*
*/
public class BaseModelCollectionJSON implements BaseModelCollection {
	
	private List<BaseModel> lst;
	private Iterator<BaseModel> it;
	private boolean changed = false;
	
	public BaseModelCollectionJSON(List<BaseModel> lst){
		this.lst = new LinkedList<BaseModel>();
		this.it = this.lst.iterator();
	}
	
	public boolean hasNext() {
		return it.hasNext();
	}

	@Override
	public BaseModel next() {
		return it.next();
	}
	
	/**
	*
	* Returns a list of all elements in the bridge
	*
	* @return A list of all elements in the bridge
	*/
	public List<BaseModel> getElements() {
		List<BaseModel> elements = new LinkedList<BaseModel>();
		Iterator<BaseModel> it = lst.iterator();
		while (it.hasNext()){
			elements.add(it.next());
		}
		return elements;
	}
	
	public void add(BaseModel newBaseModel ) {
		changed = true;
		this.lst.add(newBaseModel);
	}
	
	public void add(List<BaseModel> listBaseModel) {
		changed = true;
		Iterator<BaseModel> it = listBaseModel.iterator();
		while (it.hasNext()){
			BaseModel currentBaseModelToAdd = it.next();
			add(currentBaseModelToAdd);
		}
		
	}
	
	public boolean hasBaseModel(BaseModel BaseModelToCheck){
		return lst.contains(BaseModelToCheck);
	}
	
	/**
	* Whether the collection was changed or not
	*
	* @return true if the collection was changed and false otherwise
	*/
	public boolean isChanged(){
		return changed;
	}
	
	/**
	* Whether the collection is empty
	*
	* @return true if the collection is empty and false otherwise
	*/
	public boolean isEmpty(){
		return lst.isEmpty();
	}
	
	/**
	* Retrieves the number of records in the collection
	*
	* @return the number of records in the collection 
	*/
	public long getRecordCount(){
		return lst.size();
	}
	
	@Override
	public void remove() {
		changed = true;
		this.it.remove();
	}
	
	@Override
	public Iterator<BaseModel> iterator() {
		return lst.iterator();
	}
}
	
	
