package com.corrspt.xeo.api.core.logger;

import netgest.bo.system.LoggerLevels.LoggerLevel;

public class SystemOutLogger implements Logger {

	
	private Class<?> theClass;
	
	public SystemOutLogger(Class<?> klass){
		theClass = klass;
	}
	
	private void print(String message){
		System.out.println(theClass.getName() + " - " + message);
	}
	
	@Override
	public void setLevel(LoggerLevel level) {
		
	}

	@Override
	public void log(LoggerLevel level, String message) {
		print(message);

	}

	@Override
	public void log(LoggerLevel level, String message, Object... messageArgs) {
		print(String.format(message,messageArgs));

	}

	@Override
	public void log(LoggerLevel level, String message, Throwable t) {
		print(message);
	}

	@Override
	public void log(LoggerLevel level, String message, Throwable t,
			Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

	@Override
	public boolean isFinestEnabled() {
		return true;
	}

	@Override
	public boolean isFinerEnabled() {
		return true;
	}

	@Override
	public boolean isFineEnabled() {
		return true;
	}

	@Override
	public void finest(Throwable t) {
		t.printStackTrace(System.out);
	}

	@Override
	public void finest(String message) {
		print(message);
	}

	@Override
	public void finest(String message, Throwable e) {
		print(message);
	}

	@Override
	public void finest(String message, Object... messageArgs) {
		print(String.format(message,messageArgs));

	}

	@Override
	public void finest(String message, Throwable e, Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

	@Override
	public void finer(Throwable t) {
		t.printStackTrace(System.out);
	}

	@Override
	public void finer(String message) {
		print(message);

	}

	@Override
	public void finer(String message, Throwable e) {
		print(message);
	}

	@Override
	public void finer(String message, Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

	@Override
	public void finer(String message, Throwable e, Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

	@Override
	public void fine(Throwable t) {
		t.printStackTrace(System.out);
	}

	@Override
	public void fine(String message) {
		print(message);
	}

	@Override
	public void fine(String message, Throwable e) {
		print(message);
	}

	@Override
	public void fine(String message, Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

	@Override
	public void fine(String message, Throwable e, Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

	@Override
	public void warn(Throwable t) {
		t.printStackTrace(System.out);
	}

	@Override
	public void warn(String message) {
		print(message);
	}

	@Override
	public void warn(String message, Throwable e) {
		print(message);
	}

	@Override
	public void warn(String message, Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

	@Override
	public void warn(String message, Throwable e, Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

	@Override
	public void severe(Throwable t) {
		t.printStackTrace(System.out);
	}

	@Override
	public void severe(String message) {
		print(message);

	}

	@Override
	public void severe(String message, Throwable e) {
		print(message);
	}

	@Override
	public void severe(String message, Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

	@Override
	public void severe(String message, Throwable e, Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

	@Override
	public void config(String message) {
		print(message);

	}

	@Override
	public void config(String message, Object... messageArgs) {
		print(String.format(message,messageArgs));
	}

}
