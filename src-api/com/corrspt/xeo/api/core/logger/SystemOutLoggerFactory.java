package com.corrspt.xeo.api.core.logger;

public class SystemOutLoggerFactory implements LoggerCreator {

	@Override
	public Logger getLogger(Class<?> klass) {
		return new SystemOutLogger(klass);
	}

}
