package com.corrspt.xeo.api.core.logger;

public interface LoggerCreator {
	
	public Logger getLogger(Class<?> klass);

}
