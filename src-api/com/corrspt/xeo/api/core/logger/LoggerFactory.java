package com.corrspt.xeo.api.core.logger;

/**
 * Creates Logs for a given class, can be used to modify
 *
 */
public class LoggerFactory {

	/**
	 * Current LoggerCreator
	 */
	private static LoggerCreator logger = new XeoLoggerFactory();
	
	/**
	 * 
	 * Changes the current logger factory to a new one
	 * 
	 * @param newLogger The new logger factory
	 */
	public static void changeLoggerCreator(LoggerCreator newLogger){
		logger = newLogger;
	}
	
	/**
	 * 
	 * Retrieves a logger from the factory for a given class
	 * 
	 * @param klass The class to create the logger for
	 * @return A Logger for the given class
	 */
	public static Logger getLogger(Class<?> klass){
		return logger.getLogger(klass);
	}
	
	
}
