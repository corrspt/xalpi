package com.corrspt.xeo.api.core.logger;

public class XeoLoggerFactory implements LoggerCreator {

	@Override
	public Logger getLogger(Class<?> klass) {
		return new XeoLoggerAdapter(netgest.bo.system.Logger.getLogger(klass));
	}

}
