package com.corrspt.xeo.api.core.logger;

import netgest.bo.system.LoggerLevels.LoggerLevel;

public interface Logger {
	
	
	/**
	 * Sets the logger level for this logger.
	 * 
	 * @param level the new level based on class LoggerLevels
	 */
	public void setLevel(LoggerLevel level);

	/**
	 * Log a message to the logger system
	 * 
	 * @param level the level of the message
	 * @param message String with the message to log
	 */
	public void log( LoggerLevel level, String message );

	/**
	 * Log a message to the logger system
	 * 
	 * @param level the level
	 * @param message mask of the message formated as {@link Formatter}
	 * @param messageArgs the arguments for the message
	 */
	public void log( LoggerLevel level, String message, Object... messageArgs );

	/**
	 * Log a message to the logger system
	 * 
	 * @param level the level
	 * @param message  String with the message to log
	 * @param t the base exception
	 */
	public void log( LoggerLevel level, String message, Throwable t );

	/**
	 * Log a message to the logger system
	 * 
	 * @param level the level
	 * @param message mask of the message formated as {@link Formatter}
	 * @param t the base exception
	 * @param messageArgs  the arguments for the message
	 */
	public void log( LoggerLevel level, String message, Throwable t, Object... messageArgs );
	
	/**
	 * Checks if is finest enabled.
	 * 
	 * @return true, if is finest enabled
	 */
	public boolean isFinestEnabled();

	/**
	 * Checks if is finer enabled.
	 * 
	 * @return true, if is finer enabled
	 */
	public boolean isFinerEnabled();

	/**
	 * Checks if is fine enabled.
	 * 
	 * @return true, if is fine enabled
	 */
	public boolean isFineEnabled();

	/**
	 * Log in Finest level.
	 * 
	 * @param t the base exception
	 */
	public void finest( Throwable t );

	/**
	 *  Log in Finest level.
	 * 
	 * @param message the message
	 */
	public void finest( String message );

	/**
	 *  Log in Finest level.
	 * 
	 * @param message the message
	 * @param e the base exception
	 */
	public void finest( String message, Throwable e );

	/**
	 * Log in Finest level.
	 * 
	 * @param message mask of the message formated as {@link Formatter}
	 * @param messageArgs the arguments for the message
	 */
	public void finest( String message, Object... messageArgs );

	/**
	 * Log in Finest level.
	 * 
	 * @param message mask of the message formated as {@link Formatter}
	 * @param e the base exception
	 * @param messageArgs the arguments for the message
	 */
	public void finest( String message, Throwable e, Object... messageArgs );
	

	/**
	 * Log in Finer level.
	 * 
	 * @param t the base exception
	 */
	public void finer( Throwable t ) ;
	
	/**
	 * Log in Finer level.
	 * 
	 * @param message the message
	 */
	public void finer( String message ) ;

	/**
	 * Log in Finer level.
	 * 
	 * @param message the message
	 * @param e the base exception
	 */
	public void finer( String message, Throwable e ) ;

	/**
	 * Log in Finer level.
	 * 
	 * @param message the mask of the message formated as {@link Formatter}
	 * @param messageArgs the arguments for the message
	 */
	public void finer( String message, Object... messageArgs ) ;

	/**
	 * Log in Finer level.
	 * 
	 * @param message the mask of the message formated as {@link Formatter}
	 * @param e the base exception
	 * @param messageArgs the arguments for the message
	 */
	public void finer( String message, Throwable e, Object... messageArgs ) ;
	
	/**
	 *  Log in Fine level.
	 * 
	 * @param t the base exception
	 */
	public void fine( Throwable t ) ;

	/**
	 *  Log in Fine level.
	 * 
	 * @param message the message
	 */
	public void fine( String message ) ;

	/**
	 * Log in Fine level.
	 * 
	 * @param message the message
	 * @param e the base exception
	 */
	public void fine( String message, Throwable e ) ;

	/**
	 * Log in Fine level.
	 * 
	 * @param message the mask of the message formated as {@link Formatter}
	 * @param messageArgs the message arguments
	 */
	public void fine( String message, Object... messageArgs ) ;

	/**
	 * Log in Fine level.
	 * 
	 * @param message the mask of the message formated as {@link Formatter}
	 * @param e the base exception
	 * @param messageArgs the message arguments
	 */
	public void fine( String message, Throwable e, Object... messageArgs ) ;

	/**
	 * Log in Warning level.
	 * 
	 * @param t the base exception
	 */
	public void warn( Throwable t ) ;

	/**
	 * Log in Warning level.
	 * 
	 * @param message the message
	 */
	public void warn( String message ) ;

	/**
	 * Log in Warning level.
	 * 
	 * @param message the message
	 * @param e the base exception
	 */
	public void warn( String message, Throwable e ) ;

	/**
	 * Log in Warning level.
	 * 
	 * @param message the mask of the message formated as {@link Formatter}
	 * @param messageArgs the message arguments
	 */
	public void warn( String message, Object... messageArgs );

	/**
	 * Log in Warning level.
	 * 
	 * @param message the mask of the message formated as {@link Formatter}
	 * @param e the base exception
	 * @param messageArgs the message arguments
	 */
	public void warn( String message, Throwable e, Object... messageArgs ) ;
	
	/**
	 * Log in Severe level.
	 * 
	 * @param t the base exception
	 */
	public void severe( Throwable t ) ;
	
	/**
	 *  Log in Severe level.
	 * 
	 * @param message the message
	 */
	public void severe( String message );

	/**
	 * Log in Severe level.
	 * 
	 * @param message the message
	 * @param e the base exception
	 */
	public void severe( String message, Throwable e ) ;

	/**
	 * Log in Severe level.
	 * 
	 * @param message the mask of the message formated as {@link Formatter}
	 * @param messageArgs the message arguments
	 */
	public void severe( String message, Object... messageArgs ) ;

	/**
	 * Log in Severe level.
	 * 
	 * @param message the mask of the message formated as {@link Formatter}
	 * @param e the base exception
	 * @param messageArgs the message arguments
	 */
	public void severe( String message, Throwable e, Object... messageArgs );
	
	/**
	 * Log in Config level..
	 * 
	 * @param t the t
	 */
	public void config( String message );

	/**
	 * Log in Config level..
	 * 
	 * @param message the mask of the message formated as {@link Formatter}
	 * @param messageArgs the message arguments
	 */
	public void config( String message, Object... messageArgs ) ;

	

}
