package com.corrspt.xeo.api.core.logger;

import netgest.bo.system.LoggerLevels.LoggerLevel;

public class XeoLoggerAdapter implements Logger {

	private netgest.bo.system.Logger logger;
	
	public XeoLoggerAdapter(netgest.bo.system.Logger logger){
		this.logger = logger;
	}
	
	@Override
	public void setLevel(LoggerLevel level) {
		logger.setLevel(level);
	}

	@Override
	public void log(LoggerLevel level, String message) {
		logger.log(level, message);		
	}

	@Override
	public void log(LoggerLevel level, String message, Object... messageArgs) {
		logger.log(level, message, messageArgs);
	}

	@Override
	public void log(LoggerLevel level, String message, Throwable t) {
		logger.log(level, message,t);
	}

	@Override
	public void log(LoggerLevel level, String message, Throwable t,
			Object... messageArgs) {
		logger.log(level, message,t,messageArgs);
	}

	@Override
	public boolean isFinestEnabled() {
		return logger.isFinestEnabled();
	}

	@Override
	public boolean isFinerEnabled() {
		return logger.isFinerEnabled();
	}

	@Override
	public boolean isFineEnabled() {
		return logger.isFineEnabled();
	}

	@Override
	public void finest(Throwable t) {
		if (logger.isFinestEnabled())
			logger.fine(t);
		
	}

	@Override
	public void finest(String message) {
		if (logger.isFinestEnabled())
			logger.finest(message);
	}

	@Override
	public void finest(String message, Throwable e) {
		if (logger.isFinestEnabled())
			logger.finest(message, e);
		
	}

	@Override
	public void finest(String message, Object... messageArgs) {
		if (logger.isFinestEnabled())
			logger.finest(message, messageArgs);
		
	}

	@Override
	public void finest(String message, Throwable e, Object... messageArgs) {
		if (logger.isFinestEnabled())
			logger.finest(message, e, messageArgs);
		
	}

	@Override
	public void finer(Throwable t) {
		if (logger.isFinerEnabled())
			logger.finer(t);
		
	}

	@Override
	public void finer(String message) {
		if (logger.isFinerEnabled())
			logger.finer(message);
		
	}

	@Override
	public void finer(String message, Throwable e) {
		if (logger.isFinerEnabled())
			logger.finer(message,e);
	}

	@Override
	public void finer(String message, Object... messageArgs) {
		if (logger.isFinerEnabled())
			logger.finer(message,messageArgs);
		
	}

	@Override
	public void finer(String message, Throwable e, Object... messageArgs) {
		if (logger.isFinerEnabled())
			logger.finer(message,e,messageArgs);
	}

	@Override
	public void fine(Throwable t) {
		if (logger.isFineEnabled())
			logger.fine(t);
		
	}

	@Override
	public void fine(String message) {
		if (logger.isFineEnabled())
			logger.fine(message);
		
	}

	@Override
	public void fine(String message, Throwable e) {
		if (logger.isFineEnabled())
			logger.fine(message,e);
		
	}

	@Override
	public void fine(String message, Object... messageArgs) {
		if (logger.isFineEnabled())
			logger.fine(message,messageArgs);
		
	}

	@Override
	public void fine(String message, Throwable e, Object... messageArgs) {
		if (logger.isFineEnabled())
			logger.fine(message,e,messageArgs);
		
	}

	@Override
	public void warn(Throwable t) {
		logger.warn(t);
		
	}

	@Override
	public void warn(String message) {
		logger.warn(message);
		
	}

	@Override
	public void warn(String message, Throwable e) {
		logger.warn(message,e);
		
	}

	@Override
	public void warn(String message, Object... messageArgs) {
		logger.warn(message,messageArgs);
		
	}

	@Override
	public void warn(String message, Throwable e, Object... messageArgs) {
		logger.warn(message,e,messageArgs);
		
	}

	@Override
	public void severe(Throwable t) {
		logger.severe(t);
		
	}

	@Override
	public void severe(String message) {
		logger.severe(message);
		
	}

	@Override
	public void severe(String message, Throwable e) {
		logger.severe(message, e);
		
	}

	@Override
	public void severe(String message, Object... messageArgs) {
		logger.severe(message, messageArgs);
		
	}

	@Override
	public void severe(String message, Throwable e, Object... messageArgs) {
		logger.severe(message, e, messageArgs);
		
	}

	@Override
	public void config(String message) {
		logger.config(message);
		
	}

	@Override
	public void config(String message, Object... messageArgs) {
		logger.config(message, messageArgs);
		
	}

	
	
}
