package com.corrspt.xeo.api.core;

/**
 * 
 * Implementation of the {@link BOUI} interface for objects that
 * use the sequence of long numbers as the boui
 *
 */
public class XEOObjectBoui implements BOUI {

	/**
	 * The private Long object representing the boui
	 */
	private final Long p_boui;
	
	/**
	 * 
	 * Constructor from a Long object value
	 * 
	 * @param value The BOUI
	 * 
	 */
	public XEOObjectBoui(Long value){
		assert value != null : "BOUI cannot be null";
		this.p_boui = value;
	}
	
	/**
	 * 
	 * Constructor from a long value
	 * 
	 * @param value
	 */
	public XEOObjectBoui(long value){
		assert value > 0 : "BOUI cannot be zero";
		this.p_boui = Long.valueOf(value);
	}
	
	
	/**
	 * 
	 * Constructor from a String
	 * 
	 * @param value The boui
	 */
	public XEOObjectBoui(String value){
		assert value != null : "BOUI cannot be null";
		assert value.length() > 0 : "BOUI must have value";
		this.p_boui = Long.valueOf(value);
	}
	
	@Override
	public Object getValue() {
		return new Long(p_boui);
	}

	@Override
	public String serialize() {
		return String.valueOf(p_boui);
	}

	@Override
	public boolean equals(BOUI o) {
		return String.valueOf(p_boui).equalsIgnoreCase(o.serialize());
	}

}
