package com.corrspt.xeo.api.core;

/**
 * 
 * Represents a message associated to an {@link XEOObject}
 *
 */
public class XEOObjectMessage implements Message{

	private String p_title;
	
	private String p_content;

	private MessageType p_type;
	
	private XEOObjectMessage(String title, String message, MessageType type){
		this.p_title = title;
		this.p_content = message;
		this.p_type = type;
	}
	
	/**
	 * 
	 * Get the title of the message
	 * 
	 * @return The message title
	 */
	public String geTitle(){
		return p_title;
	}
	
	/**
	 * 
	 * Get the content of the message
	 * 
	 * @return The message content
	 */
	public String getContent(){
		return p_content;
	}
		
	
	/**
	 * 
	 * Retrieve the message 
	 * 
	 * @return
	 */
	public MessageType getType(){
		return p_type;
	}
	
	
	/**
	 * 
	 * Create a new Info message to add to an XEO Object
	 * 
	 * @param title The title of the message
	 * @param content The message content
	 * 
	 * @return An info message
	 */
	public static XEOObjectMessage createInfoMessage(String title, String content){
		assert title != null : "Title cannot be null";
		assert content != null : "Content cannot be null";
		assert content.length() > 0 : "Must have content";
		return new XEOObjectMessage(title,content,MessageType.INFO);
	}
	
	/**
	 * 
	 * Create a new Error message to add to an XEO Object
	 * 
	 * @param title The title of the message
	 * @param content The message content
	 * 
	 * @return An Error message
	 */
	public static XEOObjectMessage createErrorMessage(String title, String content){
		assert title != null : "Title cannot be null";
		assert content != null : "Content cannot be null";
		assert content.length() > 0 : "Must have content";
		return new XEOObjectMessage(title,content,MessageType.ERROR);
	}
	
	/**
	 * 
	 * Create a new Critical message to add to an XEO Object
	 * 
	 * @param title The title of the message
	 * @param content The message content
	 * 
	 * @return A critical message
	 */
	public static XEOObjectMessage createCriticalMessage(String title, String content){
		assert title != null : "Title cannot be null";
		assert content != null : "Content cannot be null";
		assert content.length() > 0 : "Must have content";
		return new XEOObjectMessage(title,content,MessageType.CRITICAL);
	}
	
	
	
}
