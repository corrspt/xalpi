package com.corrspt.xeo.api.core;

/**
 * 
 * Attributes of the Patient XEO Model
 *
 */
public enum XEOObjectAtts{
	
	PARENT("Parent",""),
	PARENTCTX("ParentCtx",""),
	TEMPLATE("Template",""),
	BOUI("BOUI",""),
	CLASSNAME("Classname",""),
	CREATOR("Creator",""),
	SYS_DTCREATE("SYS_DTCREATE",""),
	SYS_DTSAVE("SYS_DTSAVE",""),
	SYS_ORIGIN("SYS_ORIGIN",""),
	SYS_FROMOBJ("SYS_FROMOBJ",""),
	;
	
	/**
	 * Stores the description of the attribute
	 */
	private String description;
	
	/**
	 * Stores the label of the attribute
	 */
	private String label;
	
	private XEOObjectAtts(String label,String description){
		this.description = description;
		this.label = label;
	}
	
	/**
	 * 
	 * Retrieves the label
	 * 
	 * @return A string with the label of the attribute
	 */
	public String getLabel(){
		return label;
	}
	/**
	 * 
	 * Retrieve the description of the attribute
	 * 
	 * @return A string with the description of the attribute
	 */
	public String getDescription(){
		return description;
	}
	
}


