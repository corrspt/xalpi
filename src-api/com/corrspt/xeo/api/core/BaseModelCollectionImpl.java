package com.corrspt.xeo.api.core;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import netgest.bo.runtime.boBridgeIterator;
import netgest.bo.runtime.boRuntimeException;
import netgest.bo.runtime.bridgeHandler;

/**
*
* Base Model
*
*/
public class BaseModelCollectionImpl implements BaseModelCollection {
	
	bridgeHandler bridge;
	boBridgeIterator it;
	
	public BaseModelCollectionImpl(bridgeHandler it){
		this.bridge = it;
		this.it = this.bridge.iterator();
	}
	
	public boolean hasNext() {
		return it.next();
	}

	@Override
	public BaseModel next() {
		try {
			return new XEOObject(it.currentRow().getObject());
		} catch (boRuntimeException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	*
	* Returns a list of all elements in the bridge
	*
	* @return A list of all elements in the bridge
	*/
	public List<BaseModel> getElements() {
		List<BaseModel> elements = new LinkedList<BaseModel>();
		try {
			boBridgeIterator it = bridge.iterator();
			while (it.next()){
				elements.add(new XEOObject(it.currentRow().getObject()));
			}
			return elements;
		} catch (boRuntimeException e) {
			e.printStackTrace();
		}
		return elements;
	}
	
	public void add(BaseModel newModel )  {
		try {
			this.bridge.add(newModel.getOriginal().getBoui());
		} catch (boRuntimeException e) {
			e.printStackTrace();
		}
	}
	
	public void add(List<BaseModel> list)  {
		Iterator<BaseModel> it = list.iterator();
		while (it.hasNext()){
			BaseModel modelToAdd = it.next();
			add(modelToAdd);
		}
		
	}
	
	public boolean hasBaseModel(BaseModel modelToCheck){
		return bridge.haveBoui(modelToCheck.getOriginal().getBoui());
	}
	
	/**
	* Whether the collection was changed or not
	*
	* @return true if the collection was chnaged and false otherwise
	*/
	public boolean isChanged(){
		return bridge.isChanged();
	}
	
	/**
	* Whether the collection is empty
	*
	* @return true if the collection is empty and false otherwise
	*/
	public boolean isEmpty(){
		return bridge.isEmpty();
	}
	
	/**
	* Retrieves the number of records in the collection
	*
	* @return the number of records in the collection 
	*/
	public long getRecordCount(){
		return bridge.getRecordCount();
	}
	
	@Override
	public void remove() {
		try {
			this.bridge.moveTo(this.it.currentRow().getLine());
			this.bridge.remove();
		} catch (boRuntimeException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Iterator<BaseModel> iterator() {
		return this;
	}
}
	
	
