package com.corrspt.xeo.api.core;

public interface AttributeEnum {
	
	public String getDescription();
	
	public String getLabel();

}
