package com.corrspt.xeo.api.core;

import java.util.Iterator;
import java.util.List;

public interface BaseModelCollection extends Iterable<BaseModel>, Iterator<BaseModel> {
	
	/**
	*
	* Returns a list of all elements in the bridge
	*
	* @return A list of all elements in the bridge
	*/
	public List<BaseModel> getElements();
	
	public void add(BaseModel newModel );
	
	public void add(List<BaseModel> list);
	
	public boolean hasBaseModel(BaseModel modelToCheck);
	
	/**
	* Whether the collection was changed or not
	*
	* @return true if the collection was changed and false otherwise
	*/
	public boolean isChanged();
	
	/**
	* Whether the collection is empty
	*
	* @return true if the collection is empty and false otherwise
	*/
	public boolean isEmpty();
	
	/**
	* Retrieves the number of records in the collection
	*
	* @return the number of records in the collection 
	*/
	public long getRecordCount();

}
