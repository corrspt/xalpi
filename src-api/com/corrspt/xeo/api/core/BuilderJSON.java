package com.corrspt.xeo.api.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import netgest.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BuilderJSON {
	
	protected JSONArray database;
	protected String where = "";
	protected List<String> args = new LinkedList<String>();
	protected String modelName = "";
	
	public BuilderJSON(JSONArray database, String modelName){
		this.database = database;
		this.modelName = modelName;
	}
	
	public BuilderJSON(JSONArray database, String modelName, String where){
		this(database,modelName);
		this.where = where;
	}
	
	public BuilderJSON(JSONArray database, String modelName, String where, List<String> args){
		this(database, modelName, where);
		this.args = args;
	}
	
	public void setWhere(String where){
		this.where = where;
	}
	
	private final DateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
	private final DateFormat formatDateTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");
	
	protected String convertDateToString(Date date){
		try {
			String result = formatDateTime.format(date);
			return result;
		} catch (IllegalArgumentException e){
			try {
				String result = formatDate.format(date);
				return result;
			} catch (IllegalArgumentException e1){
				
			}
		} 
		return null;
	}
	
	public void setArgs(Object[] args){
		for (Object arg : args){
			String value = "";
			if (arg instanceof Long){
				value = arg.toString();
			} else if (arg instanceof Date){
				value = convertDateToString((Date)arg);
			} else if (arg instanceof BaseModel){
				value = ((BaseModel) arg).getBoui().serialize();
			} else {
				value = arg.toString();
			}
			this.args.add(value);
		}
	}
	
	public JSONArray getList(){
		
		int count = 0;
		JSONArray first = new JSONArray();
		if (StringUtils.hasValue(modelName)){
			@SuppressWarnings("rawtypes")
			Iterator it = database.collection().iterator();
			while (it.hasNext()){
				JSONObject current = (JSONObject) it.next();
				count++;
				try {
					first = current.getJSONArray("data");
					String modelName = current.getString("modelName");
					if (isSameModel(modelName)){
						if (hasWhereCondition() && current.has("where")){
							String where = current.getString("where");
							if (isSameWhereCondition(where)){
								if (bothHaveArguments(current)){
									if (argumentsHaveSameValues(current))
										return current.getJSONArray("data");	
								} else if (neitherHasArguments(current)) {
									return current.getJSONArray("data");
								}
							}	 
						} else if (neitherHaveWhereConditions(current)){
							return current.getJSONArray("data");
						}


					}
				} catch (JSONException e){
					e.printStackTrace();
				}

			}
		} 
		if (onlyOneListInDataset(count, first))
			return first;
		
		return new JSONArray();
		
	}

	boolean onlyOneListInDataset(int count, JSONArray first) {
		return count == 1 && first != null;
	}

	@SuppressWarnings("rawtypes")
	private boolean argumentsHaveSameValues(JSONObject current) throws JSONException {
		int count = 0;
		JSONObject args = current.getJSONObject("args");
		for (Iterator keys = args.keys() ; keys.hasNext(); ){
			String key = (String) keys.next();
			String value = args.getString(key);
			String valueArgument = (String) this.args.get(count);
			if (!valueArgument.trim().equalsIgnoreCase(value))
				return false;
			count++;
		}
		return true;
	}


	boolean neitherHasArguments(JSONObject current) throws JSONException {
		return args.isEmpty() && (!current.has("args") || current.getJSONObject("args").length() == 0);
	}
	
	boolean bothHaveArguments(JSONObject current) throws JSONException {
		return !args.isEmpty() && (current.has("args") && current.getJSONObject("args").length() > 0);
	}


	boolean neitherHaveWhereConditions(JSONObject current) {
		return StringUtils.isEmpty(where) && !current.has("where");
	}


	boolean hasWhereCondition() {
		return StringUtils.hasValue(where);
	}


	boolean isSameModel(String modelName) {
		return this.modelName.equalsIgnoreCase(modelName);
	}


	boolean isSameWhereCondition(String where) {
		return this.where.equalsIgnoreCase(where);
	}

}
