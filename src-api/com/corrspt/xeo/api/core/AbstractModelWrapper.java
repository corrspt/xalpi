package com.corrspt.xeo.api.core;

import java.util.Date;

import netgest.bo.ejb.impl.boManagerBean;
import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boObject;
import netgest.bo.runtime.boRuntimeException;
import netgest.bo.system.boApplication;

import com.corrspt.xeo.api.core.logger.Logger;
import com.corrspt.xeo.api.core.logger.LoggerFactory;
import com.corrspt.xeo.framework.XEO;

/**
 * 
 * Base class for the wrappers around instances XEO Models
 * 
 *
 */
public class AbstractModelWrapper implements BaseModel {
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractModelWrapper.class);
	
	protected void checkEboContextIsActive(){
		if (object.getEboContext() == null){
			try {
				EboContext currentCtx = boApplication.currentContext().getEboContext();
				object = new boManagerBean().loadObject( currentCtx , object.getBoui());
			} catch (boRuntimeException e) {
				logger.warn("Could not preload object %s ",e, object.getBoui());
			}
		}
	}
	
	/**
	 * 
	 * Enumeration of System Attributes
	 * 
	 */
	public static enum SystemAtts{
		BOUI,
		SYS_DTSAVE,
		SYS_DTCREATE,
		PARENT,
		CLASSNAME
	}
	
	/**
	 * The 
	 */
	private BOUI identifier;
	
	/**
	 * The instance to wrap
	 */
	protected boObject object;
	
	
	/**
	 * 
	 * Constructor from a given instance
	 * 
	 * @param obj The instance to wrap
	 */
	protected AbstractModelWrapper(boObject obj){
		assert obj != null : "Instance cannot be null";
		this.object = obj;
		this.identifier = new XEOObjectBoui(this.object.getBoui());
	}
	
	
	/**
	 * 
	 * Constructor from a BOUI
	 * 
	 * @param boui The boui to load from
	 * @param ctx The EboContext
	 */
	protected AbstractModelWrapper(long boui, EboContext ctx){
		assert boui > 0 : "Boui cannot be zero";
		try {
			this.object = new boManagerBean().loadObject(ctx, boui);
		} catch (boRuntimeException e) {
			logger.warn("Error loading object %s", e, boui);
		}
		this.identifier = new XEOObjectBoui(boui);
	}
	
	
	/**
	 * 
	 * Checks if the object is loaded and if not,
	 * attempts to load it
	 * 
	 * @throws boRuntimeException
	 */
	protected void checkObjectLoaded() {
		if (object == null){
			try {
				EboContext currentCtx = boApplication.currentContext().getEboContext();
				object = XEO.getObjectProvider().loadObject(currentCtx, Long.valueOf(identifier.serialize()));
			} catch (NumberFormatException e) {
				logger.warn("Could not parse %s", e, identifier.serialize());
			} catch (boRuntimeException e) {
				logger.warn("Could not load object ", e);
			}
		}
		checkEboContextIsActive();
			
	}
	
	/**
	 * 
	 * Saves the instance
	 * 
	 * @throws boRuntimeException
	 */
	public void update() throws boRuntimeException {
		checkObjectLoaded();
		object.update();
	}
	
	/**
	 * 
	 * Deletes this instance
	 * 
	 * @throws boRuntimeException
	 */
	public void destroy() throws boRuntimeException {
		checkObjectLoaded();
		object.destroy();
	}
	
	/**
	 * 
	 * Retrieves the original instance
	 * 
	 * @return The original {@link boObject} instance
	 */
	public boObject getOriginal(){
		checkObjectLoaded();
		return object;
	}
	
	/**
	 * 
	 * Retrieves the textual card id
	 * 
	 * @return  A Strgin with the card id
	 * 
	 * @throws boRuntimeException
	 */
	public String getCardId() {
		checkObjectLoaded();
		try {
			return object.getTextCARDID().toString();
		} catch (boRuntimeException e) {
			logger.warn(e);
		}
		return "";
	}
	
	/**
	 * 
	 * Retrieves the CardId with the icon as a string
	 * 
	 * @return An Html String with the icon and the Card id
	 * 
	 * @throws boRuntimeException
	 */
	public String getCardIdIcon() {
		checkObjectLoaded();
		return "<img width='16' height='16' alt='' src='resources/"+object.getName()
			+"/ico16.gif' />" + getCardId().toString();
	}
	
	/**
	 * 
	 * Retrieve the name of the Model
	 * 
	 * @return A string with the name of the model
	 */
	public String getXeoModelName(){
		return object.getName();
	}
	
	@Override
	public String toString(){
		try{
			checkObjectLoaded();
			return object.getName() + " : "+ object.getBoui() + " : " + object.getTextCARDID();
		} catch (boRuntimeException e){
			return "Error" + object.getBoui();
		}
	}
	
	
	/**
	 * 
	 * Adds a new error message to the instance
	 * 
	 * @param errorMessage The error message
	 */
	public void addErrorMessage(String errorMessage){
		object.addErrorMessage(errorMessage);
	}
	
	/**
	 * 
	 * Checks whether the instance has errors (either object or attribute errors)
	 * 
	 * @return True if the instance has errors and false otherwise
	 */
	public boolean hasErrors(){
		return object.haveErrors();
	}
	
	/**
	 * 
	 * Checks whether the instance has errors in its attributes
	 * 
	 * @return True if any error has an attribute and false otherwise
	 */
	public boolean hasAttributeErrors(){
		return object.haveAttributeErrors();
	}
	
	/**
	 * 
	 * Checks whether there are errors in the instance (does not include attribute errors)
	 * 
	 * @return True if the instance (not its attribute) has errors
	 */
	public boolean hasErrorsInObjectOnly(){
		return object.haveObjectErrors();
	}
	
	/**
	 * 
	 * Checks whether the object is persisted in the database or not
	 * 
	 * @return True if the object exists in the database and false otherwise
	 */
	public boolean exists()  {
		checkObjectLoaded();
		try {
			return object.exists();
		} catch (boRuntimeException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Clears the list of errors (instance and object) 
	 */
	public void clearErrors()
    {
		object.clearErrors();
    }
    /**
     * Clears errors of the instance
     */
    public void clearObjectErrors()
    {
        object.clearObjectErrors();
    }
    /**
     * Clears all errors associated to attributes
     */
    public void clearAttributeErrors()
    {
        object.clearAttributeErrors();
    }
    
    /**
     * 
     * Whether the instance was changed since it was first loaded
     * 
     * @return True if the instance was changed since it was first loaded
     * and false otherwise
     */
    public boolean isChanged()  {
    	checkObjectLoaded();
    	try {
			return object.isChanged();
		} catch (boRuntimeException e) {
			e.printStackTrace();
		}
		return false;
    }
    
    /**
     * 
     * Disables all attributes in the instance
     * 
     * @throws boRuntimeException
     */
    public void disable() {
    	checkObjectLoaded();
    	try {
			object.setDisabled();
		} catch (boRuntimeException e) {
			logger.warn(e);
		}
    }
    
    /**
     * 
     * Checks whether the instance is disabled not
     * 
     * @return True if the instance is disabled and false otherwise
     */
    public boolean isDisabled(){
    	return object.isDisabled();
    }
    
    /**
     * Enables the instance (sets all attributes to enabled)
     */
    public void enable() {
    	checkObjectLoaded();
    	try {
			object.setEnabled();
		} catch (boRuntimeException e) {
			logger.warn(e);
		}
    }
    

    /**
     * Checks whether the instance is enabled or not
     * 
     * @return True if the instance is enabled and false otherwise
     */
    public boolean isEnabled(){
    	return object.isEnabled;
    }
    
    
    /**
     * 
     * Sets the parent for this instance
     * 
     * @param parent The parent
     */
    public void setParent(BaseModel parent) {
    	checkObjectLoaded();
    	try {
			object.addParent(parent.getOriginal());
		} catch (boRuntimeException e) {
			logger.warn(e);
		}
    }
    
    /**
     * Removes the parent from this instance
     */
    public void removeParent(){
    	checkObjectLoaded();
    	try {
			object.removeParent(null, false);
		} catch (boRuntimeException e) {
			logger.warn(e);
		}
    }
    
    /**
     * 
     * Retrieves the EboContext associated to this object
     * 
     * @return The EboContext
     */
    public EboContext getEboContext(){
    	checkObjectLoaded();
    	return object.getEboContext();
    }
    
    /**
     * 
     * Retrieves the BOUI of the instance
     * 
     * @return 
     */
    public BOUI getBoui(){
    	return identifier;
    }
    
    
    
    /**
     * 
     * Retrieve the date of creation for this object
     * 
     * @return
     * 
     * @throws boRuntimeException
     */
    public Date getDateCreate() {
    	checkObjectLoaded();
    	try {
			return object.getAttribute(SystemAtts.SYS_DTCREATE.name()).getValueDate();
		} catch (boRuntimeException e) {
			logger.warn(e);
		}
		return null;
    }
    
    /**
     * 
     * Retrieve the date of the lastupdate to this instance
     * 
     * @return
     * @throws boRuntimeException
     */
    public Date getDateLastUpdate() {
    	checkObjectLoaded();
    	try {
			return object.getAttribute(SystemAtts.SYS_DTSAVE.name()).getValueDate();
		} catch (boRuntimeException e) {
			logger.warn(e);
		}
		return null;
    }
    
    
    
    /**
     * 
     * Checks whether this object has a parent object
     * 
     * @return True if the object has a parent and false otherwise
     * 
     * @throws boRuntimeException
     */
    public boolean hasParent() {
    	checkObjectLoaded();
    	try {
			return object.getParent() != null;
		} catch (boRuntimeException e) {
			logger.warn(e);
		}
		return false;
    }
    
    /**
     * 
     * Returns the parent object for this instance
     * 
     * @return The parent of this instance
     */
    public BaseModel getParent() {
    	checkObjectLoaded();
    	boObject parent;
		try {
			parent = object.getParent();
			if (parent != null)
	    		return new XEOObject(parent);
		} catch (boRuntimeException e) {
			logger.warn(e);
		}
    	return null;
    }
    
    
    public boolean isSame(BaseModel model){
    	checkObjectLoaded();
    	if (object.getBoui() == model.getOriginal().getBoui())
    		return true;
    	
    	return false;
    }
    
    
    public boolean isValid(){
    	checkObjectLoaded();
    	try {
			return object.valid();
		} catch (boRuntimeException e) {
			logger.warn(e);
		}
		return false;
    }
    
    public void setCustomParameter(String name, String value){
    	checkObjectLoaded();
    	object.setParameter(name, value);
    	object.poolSetStateFull();
    }
    
    public String getCustomParameter(String name){
    	checkObjectLoaded();
    	return object.getParameter(name);
    }


}
