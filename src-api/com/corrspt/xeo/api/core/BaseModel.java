package com.corrspt.xeo.api.core;

import java.util.Date;

import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boObject;
import netgest.bo.runtime.boRuntimeException;

public interface BaseModel {
	
	/**
	 * 
	 * Saves the instance
	 * 
	 * @throws boRuntimeException
	 */
	public void update() throws boRuntimeException;
	
	/**
	 * 
	 * Deletes this instance
	 * 
	 * @throws boRuntimeException
	 */
	public void destroy() throws boRuntimeException;
	
	/**
	 * 
	 * Retrieves the original instance
	 * 
	 * @return The original {@link boObject} instance
	 */
	public boObject getOriginal();
	
	/**
	 * 
	 * Retrieves the textual card id
	 * 
	 * @return  A String with the card id
	 * 
	 */
	public String getCardId() ;
	
	/**
	 * 
	 * Retrieves the CardId with the icon as a string
	 * 
	 * @return An Html String with the icon and the Card id
	 * 
	 */
	public String getCardIdIcon() ;
	
	/**
	 * 
	 * Retrieve the name of the Model
	 * 
	 * @return A string with the name of the model
	 */
	public String getXeoModelName();
	
	
	/**
	 * 
	 * Adds a new error message to the instance
	 * 
	 * @param errorMessage The error message
	 */
	public void addErrorMessage(String errorMessage);
	
	
	/**
	 * 
	 * Checks whether the instance has errors (either object or attribute errors)
	 * 
	 * @return True if the instance has errors and false otherwise
	 */
	public boolean hasErrors();
	
	/**
	 * 
	 * Checks whether the instance has errors in its attributes
	 * 
	 * @return True if any error has an attribute and false otherwise
	 */
	public boolean hasAttributeErrors();
	
	/**
	 * 
	 * Checks whether there are errors in the instance (does not include attribute errors)
	 * 
	 * @return True if the instance (not its attribute) has errors
	 */
	public boolean hasErrorsInObjectOnly();
	
	/**
	 * 
	 * Checks whether the object is persisted in the database or not
	 * 
	 * @return True if the object exists in the database and false otherwise
	 */
	public boolean exists() ;
	
	/**
	 * Clears the list of errors (instance and object) 
	 */
	public void clearErrors();
    /**
     * Clears errors of the instance
     */
    public void clearObjectErrors();
    /**
     * Clears all errors associated to attributes
     */
    public void clearAttributeErrors();
    
    /**
     * 
     * Whether the instance was changed since it was first loaded
     * 
     * @return True if the instance was changed since it was first loaded
     * and false otherwise
     */
    public boolean isChanged();
    
    /**
     * 
     * Disables all attributes in the instance
     * 
     * 
     */
    public void disable();
    
    /**
     * 
     * Checks whether the instance is disabled not
     * 
     * @return True if the instance is disabled and false otherwise
     */
    public boolean isDisabled();
    
    /**
     * Enables the instance (sets all attributes to enabled)
     */
    public void enable();
    

    /**
     * Checks whether the instance is enabled or not
     * 
     * @return True if the instance is enabled and false otherwise
     */
    public boolean isEnabled();
    
    /**
     * 
     * Sets the parent for this instance
     * 
     * @param parent The parent
     */
    public void setParent(BaseModel parent);
    
    
    /**
     * Removes the current parent of the instance
     */
    public void removeParent();
    
    /**
     * 
     * Retrieves the EboContext associated to this object
     * 
     * @return The EboContext
     */
    public EboContext getEboContext();
    
    /**
     * 
     * Retrieves the BOUI of the instance
     * 
     * @return 
     */
    public BOUI getBoui();
    
    /**
     * 
     * Retrieve the date of creation for this object
     * 
     * 
     */
    public Date getDateCreate();
    
    /**
     * 
     * Retrieve the date of the last update to this instance
     * 
     * 
     */
    public Date getDateLastUpdate();
    
    
    
    /**
     * 
     * Checks whether this object has a parent object
     * 
     * @return True if the object has a parent and false otherwise
     * 
     * 
     */
    public boolean hasParent();
    
    /**
     * 
     * Returns the parent object for this instance
     * 
     * @return The parent of this instance
     */
    public BaseModel getParent();
    
    
	/**
	 * 
	 * Whether the instance is the same as the passed one 
	 * 
	 * @param o The instance to compare
	 * 
	 * @return True if the instances are the same 
	 */
	public boolean isSame(BaseModel o);

	
	/**
	 * 
	 * Whether the instance is valid or not
	 * 
	 * @return True if the instance is valid and false otherwise
	 */
	public boolean isValid();
	
	
	/**
	 * 
	 * Sets a named parameter with a given value in this instance
	 * 
	 * @param name The name of the parameter
	 * @param value The parameter value
	 */
	public void setCustomParameter(String name, String value);
	
	/**
	 * Retrieves the value associated with a custom parameter
	 * 
	 * @param name The custom parameter name
	 * @return The value associated with the name (or null if no value exists for that name)
	 */
	public String getCustomParameter(String name);
}
