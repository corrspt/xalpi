package com.corrspt.xeo.api.core;

/**
 * 
 * Represents a message added to an XEO Object
 *
 */
public interface Message {
	
	/**
	 * Type of message
	 *
	 */
	public enum MessageType{
		INFO,
		ERROR,
		CRITICAL
	}
	
	/**
	 * The message title
	 * 
	 * @return The title of the message
	 */
	public String geTitle();

	/**
	 * The message content
	 * 
	 * @return The content of the message
	 */
	public String getContent();
	
	/**
	 * 
	 * The message type
	 * 
	 * @return The message type
	 */
	public MessageType getType();
	
}
