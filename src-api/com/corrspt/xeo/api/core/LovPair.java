package com.corrspt.xeo.api.core;

/**
 * 
 * Represents a key/value pair in a List of Values
 *
 */
public interface LovPair {

	/**
	 * 
	 * The label of the pair
	 * 
	 * @return
	 */
	public String getLabel();
	
	/**
	 * 
	 * The value of the pair
	 * 
	 * @return
	 */
	public String getValue();
	
	/**
	 * Represents the NULL value
	 */
	public static final LovPair NULL = new LovPair() {
		
		@Override
		public String getValue() {
			return null;
		}
		
		@Override
		public String getLabel() {
			return null;
		}
	};
	
}
