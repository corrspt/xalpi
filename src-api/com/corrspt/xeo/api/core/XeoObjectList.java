package com.corrspt.xeo.api.core;

import java.util.Comparator;

import netgest.bo.data.DataResultSet;
import netgest.bo.def.boDefHandler;
import netgest.bo.ql.QLParser;
import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boObject;
import netgest.bo.runtime.boObjectContainer;
import netgest.bo.runtime.boObjectList;
import netgest.bo.runtime.boObjectList.SqlField;
import netgest.bo.runtime.boRuntimeException;
import netgest.utils.ParametersHandler;

import com.corrspt.xeo.framework.ObjectList;

public class XeoObjectList implements ObjectList {

	boObjectList list;
	
	public XeoObjectList(boObjectList list){
		this.list = list;
	}
	
	public EboContext getEboContext() {
		return list.getEboContext();
	}

	public void setEboContext(EboContext boctx) {
		list.setEboContext(boctx);
	}

	public byte getFormat() {
		return list.getFormat();
	}

	public QLParser getQLParser() {
		return list.getQLParser();
	}

	public boDefHandler getSelectedBoDef() throws boRuntimeException {
		return list.getSelectedBoDef();
	}

	public String getName() {
		return list.getName();
	}

	public boObject getParent() {
		return list.getParent();
	}

	public String getParentAtributeName() {
		return list.getParentAtributeName();
	}

	public boolean currentObjectIsSelected() {
		return list.currentObjectIsSelected();
	}

	public long getCurrentBoui() {
		return list.getCurrentBoui();
	}

	public void setRowProperty(String property, String value) {
		list.setRowProperty(property, value);
	}

	public String getRowProperty(String propertyName) {
		return list.getRowProperty(propertyName);
	}

	public String getSearchLetter() {
		return list.getSearchLetter();
	}

	public String getUserQuery() {
		return list.getUserQuery();
	}

	public Object[] getUserQueryArgs() {
		return list.getUserQueryArgs();
	}

	public String getBOQL() {
		return list.getBOQL();
	}

	public Object[] getBOQLArgs() {
		return list.getBOQLArgs();
	}

	public void setUserQuery(String userQuery, Object[] userQueryParam) {
		list.setUserQuery(userQuery, userQueryParam);
	}

	public boolean haveMorePages() {
		return list.haveMorePages();
	}

	public void refreshData() {
		list.refreshData();
	}

	public boolean onlyOne() {
		return list.onlyOne();
	}

	public boolean isEmpty() {
		return list.isEmpty();
	}

	public void setFilter(String filter) {
		list.setFilter(filter);
	}

	public void removeFilter() {
		list.removeFilter();
	}

	public String getFilter() {
		return list.getFilter();
	}

	public boObject getObject(long boui) throws boRuntimeException {
		return list.getObject(boui);
	}

	public boObject getObject() throws boRuntimeException {
		return list.getObject();
	}

	public void setSql(String sql) {
		list.setSql(sql);
	}

	public void setBoQl(String boql) {
		list.setBoQl(boql);
	}

	public int getRowCount() {
		return list.getRowCount();
	}

	public long getRecordCount() {
		return list.getRecordCount();
	}

	public int getPages() {
		return list.getPages();
	}

	public int getPageSize() {
		return list.getPageSize();
	}

	public int getPage() {
		return list.getPage();
	}

	public String getValueString() throws boRuntimeException {
		return list.getValueString();
	}

	public boolean next() {
		return list.next();
	}

	public boolean moveTo(int recno) {
		return list.moveTo(recno);
	}

	public int getRow() {
		return list.getRow();
	}

	public void nextPage() {
		list.nextPage();
	}

	public void previousPage() {
		list.previousPage();
	}

	public void lastPage() {
		list.lastPage();
	}

	public boolean previous() {
		return list.previous();
	}

	public void beforeFirst() {
		list.beforeFirst();
	}

	public boolean last() {
		return list.last();
	}

	public boObjectList list() {
		return list.list();
	}

	public String getOrderBy() {
		return list.getOrderBy();
	}

	public void setQueryOrderBy(String orderBy) {
		list.setQueryOrderBy(orderBy);
	}

	public boolean ordered() {
		return list.ordered();
	}

	public void setOrderBy(String column) throws boRuntimeException {
		list.setOrderBy(column);
	}

	public void poolObjectActivate() {
		list.poolObjectActivate();
	}

	public void poolObjectPassivate() {
		list.poolObjectPassivate();
	}

	public void inserRow(long boui) {
		list.inserRow(boui);
	}

	public void setObjectContainer(boObjectContainer container) {
		list.setObjectContainer(container);
	}

	public boObjectContainer getObjectContainer() {
		return list.getObjectContainer();
	}

	public ParametersHandler getParametersHandler() {
		return list.getParametersHandler();
	}

	public String[] getParametersNames() {
		return list.getParametersNames();
	}

	public String[] getParametersValues() {
		return list.getParametersValues();
	}

	public String getParameter(String parametername) {
		return list.getParameter(parametername);
	}

	public void setParameter(String parametername, String parametervalue) {
		list.setParameter(parametername, parametervalue);
	}

	public void setParameters(String[] parametersnames,
			String[] parametersvalues) {
		list.setParameters(parametersnames, parametersvalues);
	}

	public EboContext removeEboContext() {
		return list.removeEboContext();
	}

	public void setReadOnly(boolean value) {
		list.setReadOnly(value);
	}

	public boolean isReadOnly() {
		return list.isReadOnly();
	}

	public boolean containsChangedObjects() throws boRuntimeException {
		return list.containsChangedObjects();
	}

	public void removeCurrent() throws boRuntimeException {
		list.removeCurrent();
	}

	public void add(long obj_boui) throws boRuntimeException {
		list.add(obj_boui);
	}

	public boolean havePoolChilds() {
		return list.havePoolChilds();
	}

	public DataResultSet getRslt() {
		return list.getRslt();
	}

	public String getBQL() {
		return list.getBQL();
	}

	public void moveRowTo(int row) throws boRuntimeException {
		list.moveRowTo(row);
	}

	public void BidirectionalBubbleSortAlgorithm(Comparator<?> c)
			throws boRuntimeException {
		list.BidirectionalBubbleSortAlgorithm(c);
	}

	public void selectionSort(Comparator<?> c) throws boRuntimeException {
		list.selectionSort(c);
	}

	public void InsertionSortAlgorithm(Comparator<?> c) throws boRuntimeException {
		list.InsertionSortAlgorithm(c);
	}


	public boDefHandler getBoDef() throws boRuntimeException {
		return list.getBoDef();
	}

	public void firstPage() {
		list.firstPage();
	}

	public boolean first() {
		return list.first();
	}

	public String getFullTextSearch() {
		return list.getFullTextSearch();
	}

	public void setFullTextSearch(String p_fulltext) {
		list.setFullTextSearch(p_fulltext);
	}

	public void setSqlFields(SqlField[] sqlFields) {
		list.setSqlFields(sqlFields);
	}

	public SqlField[] getSqlFields() {
		return list.getSqlFields();
	}

	public int hashCode() {
		return list.hashCode();
	}

	public boolean haveBoui(long boui) {
		return list.haveBoui(boui);
	}

	public String toString() {
		return list.toString();
	}

	


}
