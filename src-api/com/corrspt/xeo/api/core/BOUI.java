package com.corrspt.xeo.api.core;

/**
 * Represents a Business Object Unique Identifier,
 * 
 *
 */
public interface BOUI {
	
	
	/**
	 * 
	 * Retrieves the native value of the Identifier
	 * 
	 * @return The Java object representing the identifier
	 */
	public Object getValue();
	
	
	/**
	 * 
	 * Returns a serialized version of the
	 * 
	 * @return
	 */
	public String serialize();
	
	
	/**
	 * 
	 * Whether this BOUI is equal to another BOUI
	 * 
	 * @param o The boui to compare
	 * 
	 * @return True if both BOUIs are equal and false otherwise
	 */
	public boolean equals(BOUI o);

}
