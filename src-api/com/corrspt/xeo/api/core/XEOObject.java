package com.corrspt.xeo.api.core;

import netgest.bo.runtime.boObject;

/**
 * 
 * Class representing a generic object
 * 
 */
public class XEOObject extends AbstractModelWrapper {

	public XEOObject(boObject obj) {
		super(obj);
	}
	
	/**
	 * 
	 * Whether this instance is wrappable as a more specific instance 
	 * 
	 * @param modelName The model to check wrappability of this instance
	 * @return
	 */
	public boolean isWrappableIn(String modelName){
		return this.object.getName().equalsIgnoreCase(modelName);
	}
	
	
	
}
