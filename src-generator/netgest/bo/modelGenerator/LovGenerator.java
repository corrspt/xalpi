package netgest.bo.modelGenerator;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import netgest.bo.runtime.boRuntimeException;

import com.corrspt.lovs.definition.DetailsType.Item;
import com.corrspt.lovs.definition.XeoLov;
import com.corrspt.models.ModelHandler;

/**
 * 
 * 
 * Generate the LOVs
 *
 */
public class LovGenerator {

	
	private File origin;
	
	private Map<String,String> listGeneratedLovs = new HashMap<String, String>();
	
	
	private ModelGenerator generator;
	
	public LovGenerator(File origin, File destinty, ModelGenerator generator){
		this.origin = origin;
		this.generator = generator;
		ModelHandler.setFileDirectory(origin.getAbsolutePath());
	}
	
	private class LovFilter implements FileFilter{

		@Override
		public boolean accept(File pathname) {
			return pathname.getAbsolutePath().endsWith(".xeolov");
		}
		
	}
	
	/**
	 * 
	 * Generates all Lovs
	 * 
	 * @throws boRuntimeException
	 * @throws IOException
	 */
	public void generateDatabaseLovs() throws boRuntimeException, IOException{
//		boObjectList list = boObjectList.list(ctx, "select Ebo_Lov",1,500);
//		long total = list.getRecordCount();
//		int counter = 0;
//		while (list.next()){
//			boObject currentLov = list.getObject();
//			createSingleLov(currentLov);
//			counter++;
//			System.out.println(counter + " / "  + total);
//		}
	}
	
	public void generateLovs() throws Exception{
		
		File[] lovs = origin.listFiles(new LovFilter());
		long total = lovs.length;
		int counter = 0;
		for (File lov : lovs){
			XeoLov lovFile = ModelHandler.getLovDefinition(lov.getName()); 
			createSingleLov(lovFile);
			counter++;
			System.out.println(counter + " / "  + total);
		}
	}
	
	
	private void createSingleLov(XeoLov lovFile) throws Exception{

		for (com.corrspt.lovs.definition.Lov lov : lovFile.getLov()){

			Map<String,String> lookupLovs = new HashMap<String, String>();
			
			InputStream in = this.getClass().getResourceAsStream("LovTemplate");
			String template = ModelGenerator.readFile( in );
			
			String lovName = lov.getName();
			lovName = replaceInvalidName(lovName);
			
			String upperLovName = ModelGenerator.upperCaseFirstLetter(lovName);
			template = template.replaceAll("#LOV_NAME#", lovName);
			
			listGeneratedLovs.put(lovName, lovName);
			
			template = template.replaceAll("#LOV_NAME_UPPER#", upperLovName);
			
			//bridgeHandler details = lov.getBridge("details");
			StringBuilder lovEntriesBuilder = new StringBuilder(500);
			StringBuilder fillMapBuilder = new StringBuilder(500);
			StringBuilder fillPublicMapBuilder = new StringBuilder(500);
			
			//Generate Lov Values
			
			List<Item> items =lov.getDetails().getItem();
			Iterator<Item> it = items.iterator();
			while (it.hasNext()){
				
				
				Item lovDetail = it.next();
				String label = lovDetail.getLabel();
				String value = lovDetail.getValue();
				
				//Watch out for duplicates, add the value after the label
				if (!lookupLovs.containsKey(label))
					lookupLovs.put(label, label);
				else	
					label = label + "_" + value;
				
				String varName = validateLovValueAsVariableName(label.toUpperCase());
				
				lovEntriesBuilder.append(ModelGenerator.TAB);
				lovEntriesBuilder.append("public static final ");
				lovEntriesBuilder.append(upperLovName);
				lovEntriesBuilder.append("Lov ");
				lovEntriesBuilder.append(varName);
				lovEntriesBuilder.append(" = new ");
				lovEntriesBuilder.append(upperLovName);
				lovEntriesBuilder.append("Lov ");
				lovEntriesBuilder.append("(\"");
				lovEntriesBuilder.append(label.toUpperCase());
				lovEntriesBuilder.append("\",\"");
				lovEntriesBuilder.append(value);
				lovEntriesBuilder.append("\"");
				lovEntriesBuilder.append(");");
				lovEntriesBuilder.append(ModelGenerator.NEW_LINE);
				
				fillMapBuilder.append(ModelGenerator.TAB);
				fillMapBuilder.append("m.put(");
				fillMapBuilder.append(varName);
				fillMapBuilder.append(".getValue()");
				fillMapBuilder.append(",");
				fillMapBuilder.append(varName);
				fillMapBuilder.append(");");
				fillMapBuilder.append(ModelGenerator.NEW_LINE);
				
				fillPublicMapBuilder.append(ModelGenerator.TAB);
				fillPublicMapBuilder.append("m.put(\"");
				fillPublicMapBuilder.append(value);
				fillPublicMapBuilder.append("\",\"");
				fillPublicMapBuilder.append(label);
				fillPublicMapBuilder.append("\");");
				fillPublicMapBuilder.append(ModelGenerator.NEW_LINE);
			}
			
			template = template.replaceAll("#LOV_STATIC_ENTRIES#", lovEntriesBuilder.toString());
			template = template.replaceAll("#FILL_MAP#", fillMapBuilder.toString());
			template = template.replaceAll("#FILL_MAP_PUBLIC#", fillPublicMapBuilder.toString());
			template = template.replaceAll("#PACKAGE#", "package " + generator.createPackageDeclaration() + ";");
			
			generator.writeToFile(template, upperLovName + "Lov" + ".java");
		}
	
	}
	
	public static String replaceInvalidName(String lovName) {
		lovName = validateLovValueAsVariableName(lovName);
		if (lovName.indexOf("_") == 1 ){
			lovName = lovName.replace("_", "");
		}
		return lovName;
	}

	
	
	/**
	 * 
	 * Since each lov label with be used as a variable name in the lov class
	 * we have to make sure that it has a valid Java variable name
	 * 
	 * @param name The lov label to be used a variable name
	 * @return A valid variable name
	 */
	static String validateLovValueAsVariableName(String name){
		
		assert name != null : "Lov label cannot be null";
		if ( name.length() == 0 )
			return name;
		
		String result = "_";
		String firstLetter = name.substring(0, 1);
		if (firstLetter.matches("[A-Za-z_]")){
			result = "";
		}
		
		//Replace all special symbols with and underscore or a space
		name = name.replaceAll("\\.", "_");
		name = name.replaceAll(" ", "_");
		name = name.replaceAll("\\+", "_PLUS_");
		name = name.replaceAll("-", "_");
		name = name.replaceAll("&", "_AND_");
		name = name.replaceAll("#", "");
		name = name.replaceAll("\"", "");
		name = name.replaceAll("'", "");
		name = name.replaceAll("!", "");
		name = name.replaceAll("/", "");
		name = name.replaceAll("\\(", "_OP_");
		name = name.replaceAll("\\)", "_CP_");
		name = name.replaceAll("\\[", "");
		name = name.replaceAll("\\]", "");
		name = name.replaceAll("\\{", "");
		name = name.replaceAll("\\}", "");
		name = name.replaceAll("\\?", "");
		name = name.replaceAll("«", "");
		name = name.replaceAll("»", "");
		name = name.replaceAll("<", "");
		name = name.replaceAll(">", "");
		name = name.replaceAll("\\*", "");
		name = name.replaceAll("´", "");
		name = name.replaceAll("`", "");
		name = name.replaceAll("~", "");
		name = name.replaceAll("\\^", "");
		name = name.replaceAll("-", "");
		name = name.replaceAll(",", "");
		name = name.replaceAll(";", "");
		name = name.replaceAll("|", "");
		name = name.replaceAll("\\\\", "");
		name = name.replaceAll("€", "");
		name = name.replaceAll("§", "");
		name = name.replaceAll("@", "");
		name = name.replaceAll("£", "");
		name = name.replaceAll("¨", "");
		
		
		result += name;
		
		return result;
		
	}
	
	/**
	 * 
	 * Retrieves the list of generated lovs
	 * 
	 * @return
	 */
	public Map<String,String> getListOfCreatedLovs(){
		return listGeneratedLovs;
	}
	
	
}
