package netgest.bo.modelGenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import netgest.bo.def.boDefAttribute;
import netgest.bo.def.boDefHandler;
import netgest.bo.runtime.boRuntimeException;
import netgest.utils.StringUtils;

import com.corrspt.models.ModelHandler;
import com.corrspt.xeo.api.core.XEOObjectAtts;



/**
 * 
 * Generates Model Wrappers
 * 
 *
 */
/**
 * @author PedroRio
 *
 */
public class ModelGenerator {

	/**
	 * The New Line (for prettiness)
	 */
	public static final String NEW_LINE = "\n";
	
	/**
	 * Tab (for prettiness)
	 */
	public static final String TAB = "\t";
	
	public static final String DEFAULT_PACKAGE = "package com.corrspt.models.generated;";
	public static final String DEFAULT_PACKAGE_NAME = "com.corrspt.models.generated.";
	
	/**
	 * Where to write files
	 */
	private File location;
	
	private File pathToModels;
	
	/**
	 * Lookup of XEO Models
	 */
	private Map<String,boDefHandler> lookupModels;
	
	/**
	 * List of definitions to process
	 */
	private boDefHandler objDefinitions[];
	
	/**
	 * List of all Lovs generated 
	 */
	private Map<String,String> lookupGeneratedLovs;
	
	/**
	 * Where the classes should be created
	 */
	private String packageDeclaration = "";

	private boolean useInheritance;
	
	/**
	 * Filter to only accepts XEOModel Properties
	 *
	 */
	private class XeoModelFilter implements FilenameFilter {
		
		public XeoModelFilter(){
			
		}
		
		@Override
		public boolean accept(File dir, String name) {
			return name.endsWith(".xeomodel") || name.endsWith(".xeoimodel");
		}
	}
	
	public ModelGenerator(File pathToModels, File pathToWritableFolder, String packageDeclaration, boolean useInheritance){
		
			this.useInheritance = useInheritance;
			this.packageDeclaration = packageDeclaration;
			lookupModels = new HashMap<String, boDefHandler>();
			this.pathToModels = pathToModels; 
			FilenameFilter filter = new XeoModelFilter();
			File[] models = pathToModels.listFiles(filter);
			System.out.println("Path to models " + pathToModels);
			ModelHandler.setFileDirectory(pathToModels.getAbsolutePath());
			location = pathToWritableFolder;
			List<boDefHandler> handlers = new LinkedList<boDefHandler>(); 
			for (File currentModel : models){
				String modelName = filenameWithoutExtension(currentModel.getName());
				boDefHandler handler = ModelHandler.getBoDefinition(modelName); 
				handlers.add( handler  );
				lookupModels.put(handler.getName(), handler);
			}
			this.objDefinitions = handlers.toArray(new boDefHandler[handlers.size()]);
			
			
		
	}
	
	
	public ModelGenerator(boDefHandler[] objectDefinitionS, File pathToFolder, 
			Map<String,String> generatedLovs, boDefHandler[] allModels){
		this.objDefinitions = objectDefinitionS;
		this.location = pathToFolder;
		this.lookupModels = new HashMap<String, boDefHandler>((int)(allModels.length * 1.5));
		this.lookupGeneratedLovs = generatedLovs;
		for (boDefHandler currHandler : allModels){
			lookupModels.put(currHandler.getName(), currHandler);
		}
	}
	
	
	private static String filenameWithoutExtension(String name) {
		return name.substring(0,name.lastIndexOf("."));
	}

	 /**
	  * 
	  * Reads a file as a String (to read the template
	  * 
	 * @param file The path to the file
	 * @return A string with the contents of the file
	 * @throws IOException
	 */
	public static String readFile( InputStream in ) throws IOException {
			BufferedReader reader = new BufferedReader( new InputStreamReader( in ) );
		    String line  = null;
		    StringBuilder stringBuilder = new StringBuilder();
		    String ls = System.getProperty("line.separator");
		    while( ( line = reader.readLine() ) != null ) {
		        stringBuilder.append( line );
		        stringBuilder.append( ls );
		    }
		    return stringBuilder.toString();
		 }
	 
	 
	 /**
	  * Writes the content of a string to disk
	  * 
	 * @param content The content to write
	 * @param destiny Where to write
	 */
	public void writeToFile(String content, String filename) throws IOException {
		String path = this.location.getAbsolutePath();
		if (!path.endsWith(File.separator)){
			path += File.separator;
		}
		
		if (StringUtils.hasValue(packageDeclaration)){
			path += packageDeclaration.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
		}
		
		if (!path.endsWith(File.separator))
			path += File.separator;
		
		File destinationFolder = new File(path);
		if (!destinationFolder.exists())
			if (!destinationFolder.mkdirs())
				System.out.println("Could not create " + destinationFolder.getAbsolutePath());
			
		
		path += filename;
		
		Writer out = new OutputStreamWriter(new FileOutputStream(path),"UTF-8");
	    try {
	      out.write(content);
	    }
	    finally {
	      out.close();
	    }
	 }
	 
	 /**
	  * 
	  * Generates all Models
	  * 
	 * @throws 
	 */
	public void generateAll() throws Exception{
		int total = objDefinitions.length;
		int processed = 0;
		
		//Generate Lovs
		LovGenerator generator = new LovGenerator(pathToModels, location, this);
		generator.generateLovs();
		this.lookupGeneratedLovs = generator.getListOfCreatedLovs();
		
		StringBuilder selectClass = new StringBuilder(400);
		StringBuilder selectFactory = new StringBuilder(400);
		StringBuilder selectFactoryImplementation = new StringBuilder(400);
		StringBuilder selectFactoryJSON = new StringBuilder(400);
		
		for(boDefHandler currentHandler : objDefinitions){
			if (!currentHandler.getName().equalsIgnoreCase("boObject")){
				 createInterface(currentHandler);
				 createModel(currentHandler);
				 createJSONFakeImplementation(currentHandler);
				 createModelFactory(currentHandler);
				 createModelFactoryImplementation(currentHandler);
				 createModelFactoryJSON(currentHandler);
				 createCollectionInterface(currentHandler);
				 createCollectionImplementation(currentHandler);
				 createCollectionImplementationJSON(currentHandler);
				 createList(currentHandler);
				 createListImplementationJSON(currentHandler);
				 createListBuilderInterface(currentHandler);
				 createListComponents(currentHandler);
				 createListBuilderJSON(currentHandler);
				 selectClass.append(generateSelectForClass(currentHandler));
				 selectFactory.append(generateSelectForFactory(currentHandler));
				 selectFactoryImplementation.append(generateSelectForFactoryImplementation(currentHandler));
				 selectFactoryJSON.append(generateSelectForFactoryJSON(currentHandler));
			}
			processed++;
			System.out.println(processed + " / " + total);
			
			
		}
		
		//Create the class to create lists
		generateSelectFactoryInterface(selectFactory);
		generateSelectClass(selectClass);
		generateSelectFactoryXEOImplmenetation(selectFactoryImplementation);
		generateSelectFactoryJSONImplementation(selectFactoryJSON);
		
		//Generate file with Profiles
		//generateProfiles();
	 }
	
	 private void generateSelectFactoryJSONImplementation(
			StringBuilder selectFactoryJSON) throws IOException {
		 
			 InputStream in = this.getClass().getResourceAsStream("SelectFactoryJSON");
			 String template = readFile(in);

			 template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
			 template = template.replaceAll("#FIELDS#", selectFactoryJSON.toString());

			 writeToFile(template, "SelectFactoryJSON.java");
		
	}


	private void generateSelectFactoryXEOImplmenetation(
			StringBuilder selectFactoryImplementation) throws IOException {
		 	InputStream in = this.getClass().getResourceAsStream("SelectFactoryImpl");
			String template = readFile(in);
			
			template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
			template = template.replaceAll("#FIELDS#", selectFactoryImplementation.toString());
			template = template.replaceAll("#TYPE#", "Impl");
			
			writeToFile(template, "SelectFactoryImpl.java");
		
	}


	private String generateSelectForFactoryImplementation(
			boDefHandler currentHandler) {
		
		 String template = NEW_LINE + 
		 TAB + "public #MODEL_NAME#ListBuilder #MODEL_NAME#(){ " +NEW_LINE +
		 TAB + TAB + " 	return new #MODEL_NAME#ListBuilderImpl();" + NEW_LINE +
		 TAB + "}" +NEW_LINE +NEW_LINE;

		 template = template.replaceAll("#MODEL_NAME#", currentHandler.getName());
		 
		 return template;
		
	}
	
	private String generateSelectForFactoryJSON(
			boDefHandler currentHandler) {
		
		 String template = NEW_LINE + 
		 TAB + "public #MODEL_NAME#ListBuilder #MODEL_NAME#(){ " +NEW_LINE +
		 TAB + TAB + " 	return new #MODEL_NAME#ListBuilderJSON(database);" + NEW_LINE +
		 TAB + "}" +NEW_LINE +NEW_LINE;

		 template = template.replaceAll("#MODEL_NAME#", currentHandler.getName());
		 
		 return template;
		
	}


	private void generateSelectFactoryInterface(StringBuilder selectFactory) throws IOException {
		 InputStream in = this.getClass().getResourceAsStream("SelectFactoryTemplate");
		 String template = readFile(in);

		 template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
		 template = template.replaceAll("#FIELDS#", selectFactory.toString());

		 writeToFile(template, "SelectFactory.java");
		
	}
	

	private String generateSelectForFactory(boDefHandler currentHandler) {
				
			String template = NEW_LINE + 
				TAB + "public #MODEL_NAME#ListBuilder #MODEL_NAME#();" +NEW_LINE +NEW_LINE;
			
			template = template.replaceAll("#MODEL_NAME#", currentHandler.getName());
			return template;
	}


	private void createJSONFakeImplementation(boDefHandler handler) throws Exception {
		 
			InputStream in = this.getClass().getResourceAsStream("JSONTemplate");
			String template = readFile(in);
			
			template = template.replaceAll("#IMPORTS#", generateImportStatements(handler,true));
			String packageDecl = createPackageDeclaration();
			if (StringUtils.hasValue(packageDecl))
				template = template.replaceAll("#PACKAGE#", "package " + packageDecl + ";");
			else
				template = template.replaceAll("#PACKAGE#", "");
			template = template.replaceAll("#MODEL_NAME#", handler.getName());
			template = template.replaceAll("#DESCRIPTION#", handler.getDescription());
			template = template.replaceAll("#MODEL_NAME_LOWER#", handler.getName().toLowerCase());
			
			String extendsModels = getExtendsClassJSON(handler);
			template = template.replaceAll("#EXTENDS#", extendsModels);
			
			template = template.replaceAll("#PACKAGE_ONLY#", packageDecl);
			
			StringBuilder attributes = new StringBuilder(600);
			boDefAttribute[] atts = handler.getAttributesDef();
			for (boDefAttribute currAtt : atts){
				if (!isSystemAttribute(currAtt.getName()))
					attributes.append(generateJSONAttributeGetterSetter(currAtt, handler));
			}
			template = template.replaceAll("#ATTRIBUTES#", attributes.toString());
			
			writeToFile(template, handler.getName() + "JSON.java");
		 
				 
		}


	private Object generateJSONAttributeGetterSetter(boDefAttribute attribute,
			boDefHandler handler) throws Exception {
		

		String attType = attribute.getAtributeDeclaredType();
		String description =
		NEW_LINE +	
		TAB + "/*" + NEW_LINE + 	
		TAB + "* " + NEW_LINE +
		TAB + "* #DESCRIPTION# " + NEW_LINE +
		TAB + "* " + NEW_LINE +
		TAB + "*/ " + NEW_LINE;
		String template =
		TAB + "public #TYPE# get#NAME#()  { " + NEW_LINE +
		TAB + TAB + "		return get#TYPE#(#MODEL_NAME#Attributes.#NAME_NORMAL#.name()); "+ NEW_LINE +
		TAB + "}" +  NEW_LINE + NEW_LINE +
		TAB + "public void set#NAME#(#TYPE# new#NAME#)  { " + NEW_LINE +
		TAB + TAB + "  		put(#MODEL_NAME#Attributes.#NAME_NORMAL#.name(),new#NAME# ); "+ NEW_LINE +
		TAB + "}" + NEW_LINE + NEW_LINE;
		
		String name = upperCaseFirstLetter(attribute.getName());
		
		template  =  template.replaceAll("#NAME#",name );
		template  =  template.replaceAll("#NAME_NORMAL#",attribute.getName());
		
		if (hasDescription(attribute)){
			template = createJavaDoc(attribute, description, template);
		}
		
		template  =  template.replaceAll("#MODEL_NAME#",attribute.getBoDefHandler().getName());
		
		template  = template.replaceAll("#CATCH_RESULT#", "null"); 
		
		if (is(boDefAttribute.ATTRIBUTE_TEXT,attType)){
			template  = checkLovAttributeJSON(template, attribute);
		}else if (is(boDefAttribute.ATTRIBUTE_NUMBER,attType)){
			template  = checkLovAttributeJSON(template, attribute);
		}else if (is(boDefAttribute.ATTRIBUTE_DATE,attType)){
			
			template =
			TAB + "public #TYPE# get#NAME#()  { " + NEW_LINE +
			TAB + TAB + " String value = getString(#MODEL_NAME#Attributes.#NAME_NORMAL#.name()); "+ NEW_LINE +
			TAB + TAB + " if (!StringUtils.isEmpty(value)){" + NEW_LINE + 
			TAB + TAB + "	return convertStringToDate(value);" + NEW_LINE + 
			TAB + TAB + " } " +  NEW_LINE +
			TAB + TAB + " return null; " + NEW_LINE +  
			TAB + "}" +  NEW_LINE + NEW_LINE +
			TAB + "public void set#NAME#(#TYPE# new#NAME#)  { " + NEW_LINE +
			TAB + TAB + "  		put(#MODEL_NAME#Attributes.#NAME_NORMAL#.name(),convertDateToString(new#NAME#) ); "+ NEW_LINE +
			TAB + "}" + NEW_LINE + NEW_LINE;
		
			template  = template.replaceAll("#TYPE#", "Date");
			template  =  template.replaceAll("#NAME#",name );
			template  =  template.replaceAll("#NAME_NORMAL#",attribute.getName());
			template  =  template.replaceAll("#MODEL_NAME#",attribute.getBoDefHandler().getName());
			
		}else if (is(boDefAttribute.ATTRIBUTE_DATETIME,attType)){
			template  = template.replaceAll("#TYPE#", "Date");
		}else if (is(boDefAttribute.ATTRIBUTE_LONGTEXT,attType)){
			template  = template.replaceAll("#TYPE#", "String");
		}else if (is(boDefAttribute.ATTRIBUTE_BOOLEAN,attType)){
			template  = template.replaceAll("#TYPE#", "Boolean");
		}else if (is(boDefAttribute.ATTRIBUTE_BINARYDATA,attType)){
			String templateForIFile = 
				NEW_LINE +	
				TAB + "/*" + NEW_LINE + 	
				TAB + "* " + NEW_LINE +
				TAB + "* #DESCRIPTION# " + NEW_LINE +
				TAB + "* " + NEW_LINE +
				TAB + "*/ " + NEW_LINE +
				TAB + "public #TYPE# get#NAME#()  { " + NEW_LINE +
				TAB + TAB + "		return ifiles.get(#MODEL_NAME#Attributes.#NAME_NORMAL#.name()); "+ NEW_LINE +
				TAB + "}" +  NEW_LINE + NEW_LINE +
				TAB + "public void set#NAME#(#TYPE# new#NAME#)  { " + NEW_LINE +
				TAB + TAB + "  		ifiles.put(#MODEL_NAME#Attributes.#NAME_NORMAL#.name(),new#NAME# ); "+ NEW_LINE +
				TAB + "}" + NEW_LINE + NEW_LINE + 
				NEW_LINE + NEW_LINE +
				TAB + "public void set#NAME#(File new#NAME#)  { " + NEW_LINE +
				TAB + TAB + "  		ifiles.put(#MODEL_NAME#Attributes.#NAME_NORMAL#.name(), new FakeIFile(new#NAME#)); "+ NEW_LINE +
				TAB + "}" + NEW_LINE + NEW_LINE;
			templateForIFile  =  templateForIFile.replaceAll("#NAME#",name );
			templateForIFile  =  templateForIFile.replaceAll("#NAME_NORMAL#",attribute.getName());
			templateForIFile  =  templateForIFile.replaceAll("#TYPE#", "iFile");
			
			if (hasDescription(attribute)){
				templateForIFile = createJavaDoc(attribute, description, templateForIFile);
			}
			
			templateForIFile  = templateForIFile.replaceAll("#MODEL_NAME#",attribute.getBoDefHandler().getName());
			
			template = templateForIFile;
		}else if (is(boDefAttribute.ATTRIBUTE_OBJECT,attType)){
			if (lookupModels.containsKey(attribute.getReferencedObjectName()) || attribute.getReferencedObjectName().equalsIgnoreCase("boObject"))
					return generateObjectRelationJSON(attribute,handler); //Só relacionar com objectos que existem
			else return "";
		}else if (is(boDefAttribute.ATTRIBUTE_SEQUENCE,attType)){
			template  = template.replaceAll("#TYPE#", "Long");
		} else if (is(boDefAttribute.ATTRIBUTE_OBJECTCOLLECTION,attType)){
			return generateBridgeJSON(attribute);
		} else if (attribute.getAtributeType() == boDefAttribute.TYPE_STATEATTRIBUTE){
			return ""; //Don't add state attributes
		}	else if (is(boDefAttribute.ATTRIBUTE_DURATION,attType)){
			template  = template.replaceAll("#TYPE#", "Long");
		}
		
		
		
		
		return template;
		
		
	}


	private void createModelFactory(boDefHandler handler)  throws IOException {
		 	InputStream in = this.getClass().getResourceAsStream("FactoryTemplate");
			String template = readFile( in );
			
			template = template.replaceAll("#IMPORTS#", "import " + createPackageDeclarationForImport() + handler.getName()+";");
			template = template.replaceAll("#MODEL_NAME#", handler.getName());
			template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
			template = template.replaceAll("#MODEL_LABEL#", handler.getLabel());
			
			writeToFile(template, handler.getName()+"Factory" + ".java");
		
	}
	 
	 private void createModelFactoryImplementation(boDefHandler handler)  throws IOException {
		 	InputStream in = this.getClass().getResourceAsStream("FactoryTemplateImplementation");
			String template = readFile( in );
			
			template = template.replaceAll("#IMPORTS#", "import " + createPackageDeclarationForImport() + handler.getName()+";");
			template = template.replaceAll("#MODEL_NAME#", handler.getName());
			template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
			template = template.replaceAll("#MODEL_LABEL#", handler.getLabel());
			
			writeToFile(template, handler.getName()+"FactoryImpl" + ".java");
		
	}
	
	 private void createModelFactoryJSON(boDefHandler handler)  throws IOException {
		 	InputStream in = this.getClass().getResourceAsStream("FactoryTemplateJSON");
			String template = readFile( in );
			
			template = template.replaceAll("#IMPORTS#", "import " + createPackageDeclarationForImport() + handler.getName()+";");
			template = template.replaceAll("#MODEL_NAME#", handler.getName());
			template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
			template = template.replaceAll("#MODEL_LABEL#", handler.getLabel());
			
			writeToFile(template, handler.getName()+"FactoryJSON" + ".java");
		
	}

	private void createListBuilderInterface(boDefHandler currentHandler) throws IOException {

		InputStream in = this.getClass().getResourceAsStream("ModelListBuilderInterfaceTemplate");
		String template = readFile(in);
		
		template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
		template = template.replaceAll("#MODEL_NAME#", currentHandler.getName());
		
		writeToFile(template, currentHandler.getName()+"ListBuilder.java");
		
		InputStream in2 = this.getClass().getResourceAsStream("WhereBuilderInterfaceTemplate");
		String template2 = readFile(in2);
		
		template2 = template2.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
		template2 = template2.replaceAll("#MODEL_NAME#", currentHandler.getName());
		
		writeToFile(template2, currentHandler.getName()+"WhereBuilder.java");
		
		InputStream in3 = this.getClass().getResourceAsStream("ModelOptionsBuilderInterfaceTemplate");
		String template3 = readFile(in3);
		
		template3 = template3.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
		template3 = template3.replaceAll("#MODEL_NAME#", currentHandler.getName());
		
		writeToFile(template3, currentHandler.getName()+"OptionsBuilder.java");
		
	}

	private void createListComponents(boDefHandler currentHandler) throws IOException {

		InputStream in = this.getClass().getResourceAsStream("ModelListBuilder");
		String template = readFile(in);
		
		template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
		template = template.replaceAll("#MODEL_NAME#", currentHandler.getName());
		
		writeToFile(template, currentHandler.getName()+"ListBuilderImpl.java");
		
		
	}
	
	private void createListBuilderJSON(boDefHandler currentHandler) throws IOException {

		InputStream in = this.getClass().getResourceAsStream("ModelListBuilderJSON");
		String template = readFile(in);
		
		template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
		template = template.replaceAll("#MODEL_NAME#", currentHandler.getName());
		
		writeToFile(template, currentHandler.getName()+"ListBuilderJSON.java");
		
		
	}
	
	


	private String generateSelectForClass(boDefHandler currentHandler) {
				
		String template = NEW_LINE + 
		TAB + "public static #MODEL_NAME#ListBuilder #MODEL_NAME#(){ " +NEW_LINE +
		TAB + TAB + " 	return selectFactory.#MODEL_NAME#();" + NEW_LINE +
		TAB + "}" +NEW_LINE +NEW_LINE;
		
		template = template.replaceAll("#MODEL_NAME#", currentHandler.getName());
		
		return template;
	}


	private void generateSelectClass(StringBuilder selectClass) throws IOException {
		
		InputStream in = this.getClass().getResourceAsStream("SelectTemplate");
		String template = readFile(in);
		
		template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
		template = template.replaceAll("#STATIC_FIELDS#", selectClass.toString());
		
		writeToFile(template, "Select.java");
		
	}


	private void createList(boDefHandler handler) throws IOException {
		createListInterface(handler);
		createListImplementation(handler);
	}


	void createListImplementation(boDefHandler handler) throws IOException {
		InputStream in = this.getClass().getResourceAsStream("ModelListTemplate");
		String template = readFile(in);
		
		template = template.replaceAll("#PACKAGE#", "package "+createPackageDeclaration()+";");
		
		template = template.replaceAll("#MODEL_NAME#", handler.getName());
		template = template.replaceAll("#IMPORTS#", "");
		
		writeToFile(template, handler.getName() + "ListImpl.java");
	}
	
	private void createListImplementationJSON(boDefHandler handler) throws IOException {
		InputStream in = this.getClass().getResourceAsStream("ModelListTemplateJSON");
		String template = readFile(in);
		
		template = template.replaceAll("#PACKAGE#", "package "+createPackageDeclaration()+";");
		
		template = template.replaceAll("#MODEL_NAME#", handler.getName());
		template = template.replaceAll("#IMPORTS#", "");
		
		writeToFile(template, handler.getName() + "ListJSON.java");
	}


	void createListInterface(boDefHandler handler) throws IOException {
		InputStream in = this.getClass().getResourceAsStream("ModelListInterface");
		String template = readFile(in);
		
		template = template.replaceAll("#PACKAGE#", "package "+createPackageDeclaration()+";");
		
		template = template.replaceAll("#MODEL_NAME#", handler.getName());
		template = template.replaceAll("#IMPORTS#", "");
		
		writeToFile(template, handler.getName() + "List.java");
	}


	void createInterface(boDefHandler handler) throws IOException {
		
		InputStream in = this.getClass().getResourceAsStream("ModelInterfaceTemplate");
		String template = readFile(in);
		
		
		
		template = template.replaceAll("#IMPORTS#", generateImportStatements(handler,false));
		String packageDecl = createPackageDeclaration();
		if (StringUtils.hasValue(packageDecl))
			template = template.replaceAll("#PACKAGE#", "package " + packageDecl + ";");
		else
			template = template.replaceAll("#PACKAGE#", "");
		template = template.replaceAll("#MODEL_NAME#", handler.getName());
		template = template.replaceAll("#DESCRIPTION#", handler.getDescription());
		
		String extendsModel = getExtendsInterface(handler);
		template = template.replaceAll("#EXTENDS#", extendsModel);
		
		StringBuilder attributes = new StringBuilder(600);
		boDefAttribute[] atts = handler.getAttributesDef();
		for (boDefAttribute currAtt : atts){
			if (!isSystemAttribute(currAtt.getName()))
				attributes.append(generateInterfaceAttributeGetterSetter(currAtt));
		}
		template = template.replaceAll("#ATTRIBUTES_INTERFACE#", attributes.toString());
		
		String enums = createModelAttsEnum(handler);
		template = template.replaceAll("#ENUM#", enums);
		
		writeToFile(template, handler.getName() + ".java");
	}



	String getExtendsInterface(boDefHandler handler) {
		String extendsModel = handler.getBoSuperBo();
		if (extendsModel.equalsIgnoreCase("boObject"))
			extendsModel = "BaseModel";
		if (useInheritance)
			return extendsModel;
		return "BaseModel";
	}
	
	private String getExtendsClass(boDefHandler handler) {
		String extendsModel = handler.getBoSuperBo();
		if (extendsModel.equalsIgnoreCase("boObject"))
			extendsModel = "XEOObject";
		else{
			if (useInheritance)
				extendsModel = extendsModel + "Impl";
			else
				extendsModel = "XEOObject";
		}
		return extendsModel;
	}
	
	private String getExtendsClassJSON(boDefHandler handler) {
		String extendsModel = handler.getBoSuperBo();
		if (extendsModel.equalsIgnoreCase("boObject"))
			extendsModel = "AbstractJSONImplementation";
		else{
			if (useInheritance)
				extendsModel = extendsModel + "JSON";
			else
				extendsModel = "AbstractJSONImplementation";
		}
		return extendsModel;
	}

	void generateProfiles() throws Exception {
		InputStream in = this.getClass().getResourceAsStream("ProfilesTemplate");
		String template = readFile(in);
		
		template = template.replaceAll("#PACKAGE#", "package " +createPackageDeclaration() + ";");
		template = template.replaceAll("#PROFILES#", listOfProfiles());
		
		//writeToFile(template, new File(location.getAbsoluteFile() + File.separator + "XEOProfiles" + ".java"));
	}
	
	/**
	 * 
	 * Generates the list of profile names for the current set
	 * 
	 * @return A string with a list of public static final Strings representing the profiles in the application
	 * @throws boRuntimeException
	 */
	private String listOfProfiles() throws boRuntimeException {
//		StringBuilder b = new StringBuilder(250);
//		boObjectList list = boObjectList.list(ctx, "select uiProfile");
//		list.beforeFirst();
//		while (list.next()){
//			boObject current = list.getObject();
//			String profileName = current.getAttribute("name").getValueString();
//			b.append("public static final String ").append(profileName.toUpperCase()).
//				append(" = \"").append(profileName).
//				append("\";").append(NEW_LINE);
//		}
//		return b.toString();
		return "";
	}

	/**
	 * 
	 * Generates a model and all of its necessary supporting structures
	 * 
	 * @param handler Handler for the object
	 * 
	 * @throws Exception
	 */
	void createModel(boDefHandler handler) throws Exception {
		
		//createModelAttsEnum(handler);
		createModelManager(handler);
		
		InputStream in = this.getClass().getResourceAsStream("ModelTemplate");
		String template = readFile(in);
		
		template = template.replaceAll("#IMPORTS#", generateImportStatements(handler,false));
		String packageDecl = createPackageDeclaration();
		if (StringUtils.hasValue(packageDecl))
			template = template.replaceAll("#PACKAGE#", "package " + packageDecl + ";");
		else
			template = template.replaceAll("#PACKAGE#", "");
		template = template.replaceAll("#MODEL_NAME#", handler.getName());
		template = template.replaceAll("#DESCRIPTION#", handler.getDescription());
		template = template.replaceAll("#MODEL_NAME_LOWER#", handler.getName().toLowerCase());
		
		String extendsModels = getExtendsClass(handler);
		template = template.replaceAll("#EXTENDS#", extendsModels);
		
		StringBuilder attributes = new StringBuilder(600);
		boDefAttribute[] atts = handler.getAttributesDef();
		for (boDefAttribute currAtt : atts){
			if (!isSystemAttribute(currAtt.getName()))
				attributes.append(generateAttributeGetterSetter(currAtt, handler));
		}
		template = template.replaceAll("#ATTRIBUTES#", attributes.toString());
		
		writeToFile(template, handler.getName() + "Impl.java");
	}

	String createPackageDeclaration() {
		if (packageDeclaration == null)
			return DEFAULT_PACKAGE;
		else
			return packageDeclaration;
	}
	
	String createPackageDeclarationForImport() {
		String pack = createPackageDeclaration();
		if (StringUtils.hasValue(pack))
			return pack + ".";
		else
			return "";
	}
	
	/**
	 * 
	 * Check if the attribute is a system attribute
	 * 
	 * @param attName The attribute to check
	 * 
	 * @return True if the attribute is a system attribute and false otherwise
	 */
	private boolean isSystemAttribute(String attName){
		try{
			XEOObjectAtts.valueOf(attName);
		} catch (IllegalArgumentException e ){
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * Returns if an attribute is of a given type
	 * 
	 * @param type The type to compare
	 * @param attType The type of the attribute
	 * @return True if the attribute is of the given type
	 */
	private boolean is(String type, String attType){
		return type.equals(attType);
	}
	
	/**
	 * 
	 * Generate the getter and setter for an attribute
	 * 
	 * @param attribute The attribute
	 * 
	 * @return Returns the string with the getter and setter
	 * 
	 * @throws IOException
	 */
	private String generateAttributeGetterSetter(boDefAttribute attribute, boDefHandler handler) throws IOException {
		String attType = attribute.getAtributeDeclaredType();
		String description =
		NEW_LINE +	
		TAB + "/*" + NEW_LINE + 	
		TAB + "* " + NEW_LINE +
		TAB + "* #DESCRIPTION# " + NEW_LINE +
		TAB + "* " + NEW_LINE +
		TAB + "*/ " + NEW_LINE;
		String template =
		TAB + "public #TYPE# get#NAME#()  { " + NEW_LINE +
		TAB + TAB + " checkObjectLoaded(); " + NEW_LINE +
		TAB + " try {" +  NEW_LINE + 
		TAB + TAB + "		return object.getAttribute(#MODEL_NAME#Attributes.#NAME_NORMAL#.name()).getValue#TYPE#(); "+ NEW_LINE +
		TAB + " } catch (boRuntimeException e) {" +
		TAB + TAB + " log.warn(e); " +  NEW_LINE +
		TAB + TAB + " return #CATCH_RESULT#; " +  NEW_LINE +
		TAB + " }" + NEW_LINE +
		TAB + "}" +  NEW_LINE + NEW_LINE +
		TAB + "public void set#NAME#(#TYPE# new#NAME#)  { " + NEW_LINE +
		TAB + TAB + " checkObjectLoaded(); " + NEW_LINE +
		TAB + " try {" + NEW_LINE + 
		TAB + TAB + "  		object.getAttribute(#MODEL_NAME#Attributes.#NAME_NORMAL#.name()).setValue#TYPE#(new#NAME#); "+ NEW_LINE +
		TAB + " } catch (boRuntimeException e) {" +  NEW_LINE +
		TAB + TAB + " log.warn(e); " +  NEW_LINE +
		TAB + " }" +
		TAB + "}" + NEW_LINE + NEW_LINE;
		
		String name = upperCaseFirstLetter(attribute.getName());
		
		template  =  template.replaceAll("#NAME#",name );
		template  =  template.replaceAll("#NAME_NORMAL#",attribute.getName());
		
		
		
		if (hasDescription(attribute)){
			template = createJavaDoc(attribute, description, template);
		}
		
		template  =  template.replaceAll("#MODEL_NAME#",attribute.getBoDefHandler().getName());
		
		template  = template.replaceAll("#CATCH_RESULT#", "null"); 
		
		if (is(boDefAttribute.ATTRIBUTE_TEXT,attType)){
			template  = checkLovAttribute(template, attribute);
		}else if (is(boDefAttribute.ATTRIBUTE_NUMBER,attType)){
			template  = checkLovAttribute(template, attribute);
		}else if (is(boDefAttribute.ATTRIBUTE_DATE,attType)){
			template  = template.replaceAll("#TYPE#", "Date");
		}else if (is(boDefAttribute.ATTRIBUTE_DATETIME,attType)){
			template  = template.replaceAll("#TYPE#", "Date");
		}else if (is(boDefAttribute.ATTRIBUTE_LONGTEXT,attType)){
			template  = template.replaceAll("#TYPE#", "String");
		}else if (is(boDefAttribute.ATTRIBUTE_BOOLEAN,attType)){
			template  = template.replaceAll("#TYPE#", "Boolean");
		}else if (is(boDefAttribute.ATTRIBUTE_BINARYDATA,attType)){
			template  = template.replaceAll("#TYPE#", "iFile");
			String templateForIFile = 
				NEW_LINE + NEW_LINE +
				TAB + "public void set#NAME#(File new#NAME#)  { " + NEW_LINE +
				TAB + TAB + " checkObjectLoaded(); " + NEW_LINE +
				TAB + " try { " +  NEW_LINE +
				TAB + TAB + "  		object.getAttribute(#MODEL_NAME#Attributes.#NAME_NORMAL#.name()).setValue#TYPE#(new FSiFile(null,new#NAME#,null)); "+ NEW_LINE +
				TAB + " } catch (boRuntimeException e) {" +  NEW_LINE +
				TAB + TAB + " log.warn(e); " +  NEW_LINE +
				TAB + " }" +
				TAB + "}" + NEW_LINE + NEW_LINE;
			templateForIFile  =  templateForIFile.replaceAll("#NAME#",name );
			templateForIFile  =  templateForIFile.replaceAll("#NAME_NORMAL#",attribute.getName());
			templateForIFile  =  templateForIFile.replaceAll("#TYPE#", "iFile");
			
			if (hasDescription(attribute)){
				templateForIFile = createJavaDoc(attribute, description, templateForIFile);
			}
			
			templateForIFile  = templateForIFile.replaceAll("#MODEL_NAME#",attribute.getBoDefHandler().getName());
			
			template += templateForIFile;
		}else if (is(boDefAttribute.ATTRIBUTE_OBJECT,attType)){
			if (lookupModels.containsKey(attribute.getReferencedObjectName()) || attribute.getReferencedObjectName().equalsIgnoreCase("boObject"))
					return generateObjectRelation(attribute,handler); //Só relacionar com objectos que existem
			else return "";
		}else if (is(boDefAttribute.ATTRIBUTE_SEQUENCE,attType)){
			template  = template.replaceAll("#TYPE#", "Long");
		} else if (is(boDefAttribute.ATTRIBUTE_OBJECTCOLLECTION,attType)){
			return generateBridge(attribute);
		} else if (attribute.getAtributeType() == boDefAttribute.TYPE_STATEATTRIBUTE){
			return ""; //Don't add state attributes
		}	else if (is(boDefAttribute.ATTRIBUTE_DURATION,attType)){
			template  = template.replaceAll("#TYPE#", "Long");
		}
		
		
		
		
		return template;
		
	}
	
	/**
	 * 
	 * Generate the getter and setter for an attribute
	 * 
	 * @param attribute The attribute
	 * 
	 * @return Returns the string with the getter and setter
	 * 
	 * @throws IOException
	 */
	private String generateInterfaceAttributeGetterSetter(boDefAttribute attribute) throws IOException {
		String attType = attribute.getAtributeDeclaredType();
		String description =
		NEW_LINE +	
		TAB + "/*" + NEW_LINE + 	
		TAB + "* " + NEW_LINE +
		TAB + "* #DESCRIPTION# " + NEW_LINE +
		TAB + "* " + NEW_LINE +
		TAB + "*/ " + NEW_LINE;
		
		String template =
		TAB + "public #TYPE# get#NAME#();" + NEW_LINE + NEW_LINE +
		TAB + "public void set#NAME#(#TYPE# new#NAME#);" + NEW_LINE + NEW_LINE;
		
		String name = upperCaseFirstLetter(attribute.getName());
		
		template  =  template.replaceAll("#NAME#",name );
		
		if (hasDescription(attribute)){
			template = createJavaDoc(attribute, description, template);
		}
		
		if (is(boDefAttribute.ATTRIBUTE_TEXT,attType)){
			template  = checkInterfaceLovAttribute(template, attribute);
		}else if (is(boDefAttribute.ATTRIBUTE_NUMBER,attType)){
			template  = checkInterfaceLovAttribute(template, attribute);
		}else if (is(boDefAttribute.ATTRIBUTE_DATE,attType)){
			template  = template.replaceAll("#TYPE#", "Date");
		}else if (is(boDefAttribute.ATTRIBUTE_DATETIME,attType)){
			template  = template.replaceAll("#TYPE#", "Date");
		}else if (is(boDefAttribute.ATTRIBUTE_LONGTEXT,attType)){
			template  = template.replaceAll("#TYPE#", "String");
		}else if (is(boDefAttribute.ATTRIBUTE_BOOLEAN,attType)){
			template  = template.replaceAll("#TYPE#", "Boolean");
		}else if (is(boDefAttribute.ATTRIBUTE_BINARYDATA,attType)){
			template  = template.replaceAll("#TYPE#", "iFile");
			String templateForIFile = 
				NEW_LINE + NEW_LINE +
				TAB + "public void set#NAME#(File new#NAME#);" + NEW_LINE + NEW_LINE;
			templateForIFile  =  templateForIFile.replaceAll("#NAME#",name );
			templateForIFile  =  templateForIFile.replaceAll("#NAME_NORMAL#",attribute.getName());
			templateForIFile  =  templateForIFile.replaceAll("#TYPE#", "iFile");
			
			if (hasDescription(attribute)){
				templateForIFile = createJavaDoc(attribute, description, templateForIFile);
			}
			
			templateForIFile  = templateForIFile.replaceAll("#MODEL_NAME#",attribute.getBoDefHandler().getName());
			
			template += templateForIFile;
		}else if (is(boDefAttribute.ATTRIBUTE_OBJECT,attType)){
			if (lookupModels.containsKey(attribute.getReferencedObjectName()) || attribute.getReferencedObjectName().equalsIgnoreCase("boObject"))
					return generateInterfaceObjectRelation(attribute); //Só relacionar com objectos que existem
			else return "";
		}else if (is(boDefAttribute.ATTRIBUTE_SEQUENCE,attType)){
			template  = template.replaceAll("#TYPE#", "Long");
		} else if (is(boDefAttribute.ATTRIBUTE_OBJECTCOLLECTION,attType)){
			return generateInterfaceBridge(attribute);
		} else if (attribute.getAtributeType() == boDefAttribute.TYPE_STATEATTRIBUTE){
			return ""; //Don't add state attributes
		}	else if (is(boDefAttribute.ATTRIBUTE_DURATION,attType)){
			template  = template.replaceAll("#TYPE#", "Long");
		}
		
		return template;
		
	}

	private String createJavaDoc(boDefAttribute attribute, String description,
			String template) {
		description  =  description.replaceAll("#DESCRIPTION#",getDocumentation(attribute));
		template = description + template;
		return template;
	}

	private boolean hasDescription(boDefAttribute attribute) {
		return StringUtils.hasValue(attribute.getDescription());
	}
	
	
	private String checkLovAttribute(String template, boDefAttribute attDefinition){
		String lovName = attDefinition.getLOVName();
		lovName = LovGenerator.replaceInvalidName(lovName);
		if (isLovAttribute(attDefinition) && lovWasGenerated(lovName)){
			String upperLovName = upperCaseFirstLetter(lovName) + "Lov";
			
			
			String description =
				NEW_LINE +	
				TAB + "/*" + NEW_LINE + 	
				TAB + "* " + NEW_LINE +
				TAB + "* #DESCRIPTION# " + NEW_LINE +
				TAB + "* " + NEW_LINE +
				TAB + "*/ ";
			
			
			template = 
				NEW_LINE +
				TAB + "public #TYPE# get#NAME#()  { " + NEW_LINE +
				TAB + TAB + " checkObjectLoaded(); " + NEW_LINE +
				TAB + " try {" +  NEW_LINE +
				TAB + TAB + "if (object.getAttribute(#MODEL_NAME#Attributes.#NAME_NORMAL#.name()).getValueString() == null)" +NEW_LINE +
				TAB + TAB + " return null;" + NEW_LINE +
				TAB + TAB + "return #TYPE#.valueOf(object.getAttribute(#MODEL_NAME#Attributes.#NAME_NORMAL#.name()).getValueString(),object.getEboContext()); "+ NEW_LINE +
				TAB + " } catch (boRuntimeException e) {" +  NEW_LINE +
				TAB + TAB + " log.warn(e); " +  NEW_LINE +
				TAB + TAB + " return null; " +  NEW_LINE +
				TAB + " }" +
				TAB + "}" +  NEW_LINE + NEW_LINE +
				TAB + "public void set#NAME#(#TYPE# new#NAME#)  { " + NEW_LINE +
				TAB + TAB + " checkObjectLoaded(); " + NEW_LINE +
				TAB + " try { " +   NEW_LINE +
				TAB + TAB + " if (new#NAME# == null){" +NEW_LINE +
				TAB + TAB + " object.getAttribute(#MODEL_NAME#Attributes.#NAME_NORMAL#.name()).setValueString(null);" +NEW_LINE +
				TAB + TAB + "} else {" +NEW_LINE +
				TAB + TAB + TAB + "object.getAttribute(#MODEL_NAME#Attributes.#NAME_NORMAL#.name()).setValueString(new#NAME#.getValue()); "+ NEW_LINE +
				TAB + TAB + "}" + NEW_LINE +
				TAB + " } catch (boRuntimeException e) {" +  NEW_LINE +
				TAB + TAB + " log.warn(e); " +  NEW_LINE + 
				TAB + " }" +
				TAB + "}"+ NEW_LINE + NEW_LINE;
			
			if (hasDescription(attDefinition)){
				template = createJavaDoc(attDefinition, description, template);
			}
			
			String name = upperCaseFirstLetter(attDefinition.getName());
			template  =  template.replaceAll("#NAME#",name );
			template  =  template.replaceAll("#NAME_NORMAL#",attDefinition.getName());
			template  =  template.replaceAll("#MODEL_NAME#",attDefinition.getBoDefHandler().getName());
			template  =  template.replaceAll("#TYPE#",upperLovName);
			
		} else {
			if (boDefAttribute.ATTRIBUTE_TEXT.equalsIgnoreCase(attDefinition.getAtributeDeclaredType())){
				template = template.replaceAll("#TYPE#", "String");
			} else {
				template = template.replaceAll("#TYPE#", "Long");
			}
		}
		return template;
	}
	
	private String checkLovAttributeJSON(String template, boDefAttribute attDefinition){
		String lovName = attDefinition.getLOVName();
		lovName = LovGenerator.replaceInvalidName(lovName);
		if (isLovAttribute(attDefinition) && lovWasGenerated(lovName)){
			String upperLovName = upperCaseFirstLetter(lovName) + "Lov";
			
			
			String description =
				NEW_LINE +	
				TAB + "/*" + NEW_LINE + 	
				TAB + "* " + NEW_LINE +
				TAB + "* #DESCRIPTION# " + NEW_LINE +
				TAB + "* " + NEW_LINE +
				TAB + "*/ ";
			
			
			template = 
				NEW_LINE +
				TAB + "public #TYPE# get#NAME#()  { " + NEW_LINE +
				
				TAB + TAB + "if (attributes.has(#MODEL_NAME#Attributes.#NAME_NORMAL#.name())){" +NEW_LINE +
				TAB + TAB + " try {  "+ NEW_LINE +  
				TAB + TAB + TAB + " String value = attributes.getString(#MODEL_NAME#Attributes.#NAME_NORMAL#.name());" + NEW_LINE +
				TAB + TAB + "return #TYPE#.valueOf(value,getEboContext()); "+ NEW_LINE +
				TAB + TAB + " } catch (JSONException e ) { e.printStackTrace(); }" + NEW_LINE + 
				TAB + TAB + " } " + NEW_LINE +
				TAB + TAB + " return null; " + NEW_LINE +
				TAB + "}" +  NEW_LINE + NEW_LINE +
				TAB + "public void set#NAME#(#TYPE# new#NAME#)  { " + NEW_LINE +
				TAB + TAB + " if (new#NAME# != null){" +NEW_LINE +
				TAB + TAB + " try {" + NEW_LINE + 
				TAB + TAB + " attributes.put(#MODEL_NAME#Attributes.#NAME_NORMAL#.name(), new#NAME#.getValue());" +NEW_LINE +
				TAB + TAB + "} catch (JSONException e ) {" +NEW_LINE +
				TAB + TAB + TAB + "e.printStackTrace(); " + NEW_LINE + 
				TAB + TAB + TAB + "}" + NEW_LINE +
				TAB + TAB + "}" + NEW_LINE +
				TAB + "}"+ NEW_LINE + NEW_LINE;
			
			if (hasDescription(attDefinition)){
				template = createJavaDoc(attDefinition, description, template);
			}
			
			String name = upperCaseFirstLetter(attDefinition.getName());
			template  =  template.replaceAll("#NAME#",name );
			template  =  template.replaceAll("#NAME_NORMAL#",attDefinition.getName());
			template  =  template.replaceAll("#MODEL_NAME#",attDefinition.getBoDefHandler().getName());
			template  =  template.replaceAll("#TYPE#",upperLovName);
			
		} else {
			if (boDefAttribute.ATTRIBUTE_TEXT.equalsIgnoreCase(attDefinition.getAtributeDeclaredType())){
				template = template.replaceAll("#TYPE#", "String");
			} else {
				template = template.replaceAll("#TYPE#", "Long");
			}
		}
		return template;
	}
	
	
	private String checkInterfaceLovAttribute(String template, boDefAttribute attDefinition){
		String lovName = attDefinition.getLOVName();
		lovName = LovGenerator.replaceInvalidName(lovName);
		if (isLovAttribute(attDefinition) && lovWasGenerated(lovName)){
			String upperLovName = upperCaseFirstLetter(lovName) + "Lov";
			
			
			String description =
				NEW_LINE +	
				TAB + "/*" + NEW_LINE + 	
				TAB + "* " + NEW_LINE +
				TAB + "* #DESCRIPTION# " + NEW_LINE +
				TAB + "* " + NEW_LINE +
				TAB + "*/ ";
			
			
			template = 
				NEW_LINE +
				TAB + "public #TYPE# get#NAME#();" +  NEW_LINE + NEW_LINE +
				TAB + "public void set#NAME#(#TYPE# new#NAME#);"+ NEW_LINE + NEW_LINE;
			
			if (hasDescription(attDefinition)){
				template = createJavaDoc(attDefinition, description, template);
			}
			
			String name = upperCaseFirstLetter(attDefinition.getName());
			template  =  template.replaceAll("#NAME#",name );
			template  =  template.replaceAll("#NAME_NORMAL#",attDefinition.getName());
			template  =  template.replaceAll("#MODEL_NAME#",attDefinition.getBoDefHandler().getName());
			template  =  template.replaceAll("#TYPE#",upperLovName);
			
		} else {
			if (boDefAttribute.ATTRIBUTE_TEXT.equalsIgnoreCase(attDefinition.getAtributeDeclaredType())){
				template = template.replaceAll("#TYPE#", "String");
			} else {
				template = template.replaceAll("#TYPE#", "Long");
			}
		}
		return template;
	}

	private boolean lovWasGenerated(String lovName) {
		return lookupGeneratedLovs.containsKey(lovName);
	}

	private boolean isLovAttribute(boDefAttribute attDefinition) {
		String lovName = attDefinition.getLOVName();
		return lovName != null && lovName.length() > 0;
	}
	
	
	/**
	 * 
	 * Takes a given text and makes the first letter upper case
	 * 
	 * @param text The text 
	 * @return The same string as input with the first character in upper case
	 */
	public static String upperCaseFirstLetter(String text){
		String name = text;
		String firstLetter = String.valueOf(name.charAt(0));
		name = firstLetter.toUpperCase() + name.substring(1,name.length());
		return name;
	}
	
	
	
	/**
	 * 
	 * Generate getter and setter for a relation attribute (1:1 relation)
	 * 
	 * @param att The relation attribute
	 * 
	 * @return A string with the getter and setter
	 */
	private String generateObjectRelation(boDefAttribute att, boDefHandler handler){
		String objectName = att.getReferencedObjectName();
		String typeName = "";
		String typeImpl = "";
		if ("boObject".equalsIgnoreCase(objectName)){
			typeName = "BaseModel";
			typeImpl = "XEOObject";
		}
		else{
			typeName = objectName;
			typeImpl = objectName + "Impl";
		}
		
		String template = NEW_LINE + 
		TAB + "public #TYPE# get#ATT_NAME_UPPER#() {"+NEW_LINE +
		TAB + TAB + " checkObjectLoaded(); " + NEW_LINE +
		TAB + " try { " + NEW_LINE +
		TAB + TAB + " if (object.getAttribute(#MODEL_NAME#Attributes.#ATT_NAME#.name()).getObject() == null)" +NEW_LINE +
		TAB + TAB + " 	return null;" + NEW_LINE +
		TAB + TAB + " return new #TYPEIMPL#(object.getAttribute(#MODEL_NAME#Attributes.#ATT_NAME#.name()).getObject());" +NEW_LINE +
		TAB + " } catch (boRuntimeException e) {" + NEW_LINE +
		TAB + TAB + " log.warn(e); " + NEW_LINE +
		TAB + TAB + " return null; " +  NEW_LINE +
		TAB + " }" +
		TAB + "}" +NEW_LINE +NEW_LINE +
		TAB + "public void set#ATT_NAME_UPPER#(#TYPE# new#TYPE#) {" +NEW_LINE +
		TAB + TAB + " checkObjectLoaded(); " + NEW_LINE +
		TAB + " try {" +  NEW_LINE +
		TAB + TAB + "if (new#TYPE# != null) " +NEW_LINE +
		TAB + TAB + TAB + "object.getAttribute(#MODEL_NAME#Attributes.#ATT_NAME#.name()).setValueLong(new#TYPE#.getOriginal().getBoui());" +NEW_LINE +
		TAB + TAB + "else " + NEW_LINE +
		TAB + TAB + TAB + "object.getAttribute(#MODEL_NAME#Attributes.#ATT_NAME#.name()).setValueObject(null); " +NEW_LINE +
		TAB + " } catch (boRuntimeException e) {" +  NEW_LINE +
		TAB + TAB + " log.warn(e); " +  NEW_LINE +
		TAB + " }" +
		TAB + "}" + NEW_LINE + NEW_LINE;
		
		
		template = template.replaceAll("#TYPE#", typeName);
		template = template.replaceAll("#TYPEIMPL#", typeImpl);
		template = template.replaceAll("#ATT_NAME_UPPER#", upperCaseFirstLetter(att.getName()));
		if (handler.getName().equalsIgnoreCase(att.getBoDefHandler().getName()))
			template = template.replaceAll("#MODEL_NAME#", att.getBoDefHandler().getName());
		else{
			System.out.println(handler.getName() + " " + att.getName() + " " + att.getBoDefHandler().getName());
			template = template.replaceAll("#MODEL_NAME#", handler.getName());
		}
		template = template.replaceAll("#ATT_NAME#", att.getName());
		
		return template;
	}
	
	/**
	 * 
	 * Generate getter and setter for a relation attribute (1:1 relation)
	 * 
	 * @param att The relation attribute
	 * 
	 * @return A string with the getter and setter
	 */
	private String generateObjectRelationJSON(boDefAttribute att, boDefHandler handler){
		String objectName = att.getReferencedObjectName();
		String typeName = "";
		String typeImpl = "";
		if ("boObject".equalsIgnoreCase(objectName)){
			typeName = "BaseModel";
			typeImpl = "XEOObject";
		}
		else{
			typeName = objectName;
			typeImpl = objectName + "Impl";
		}
		
		
		
		String template = NEW_LINE + 
		TAB + "public #TYPE# get#ATT_NAME_UPPER#() {"+NEW_LINE +
		TAB + TAB + " if (relations.containsKey(#MODEL_NAME#Attributes.#ATT_NAME#.name()))" +NEW_LINE +
		TAB + TAB + " 	return (#TYPE#) relations.get(#MODEL_NAME#Attributes.#ATT_NAME#.name());" + NEW_LINE +
		TAB + TAB + " return null;" +NEW_LINE +
		TAB + "}" +NEW_LINE +NEW_LINE +
		TAB + "public void set#ATT_NAME_UPPER#(#TYPE# new#TYPE#) {" +NEW_LINE +
		TAB + TAB + "if (new#TYPE# != null) " +NEW_LINE +
		TAB + TAB + TAB + "relations.put(#MODEL_NAME#Attributes.#ATT_NAME#.name(),new#TYPE#);" +NEW_LINE +
		TAB + "}" + NEW_LINE + NEW_LINE;
		
		
		template = template.replaceAll("#TYPE#", typeName);
		template = template.replaceAll("#TYPEIMPL#", typeImpl);
		template = template.replaceAll("#ATT_NAME_UPPER#", upperCaseFirstLetter(att.getName()));
		if (handler.getName().equalsIgnoreCase(att.getBoDefHandler().getName()))
			template = template.replaceAll("#MODEL_NAME#", att.getBoDefHandler().getName());
		else{
			System.out.println(handler.getName() + " " + att.getName() + " " + att.getBoDefHandler().getName());
			template = template.replaceAll("#MODEL_NAME#", handler.getName());
		}
		template = template.replaceAll("#ATT_NAME#", att.getName());
		
		return template;
	}
	
	private String generateInterfaceObjectRelation(boDefAttribute att){
		String objectName = att.getReferencedObjectName();
		String typeName = "";
		if ("boObject".equalsIgnoreCase(objectName))
			typeName = "BaseModel";
		else
			typeName = objectName;
		
		String template = NEW_LINE + 
		TAB + "public #TYPE# get#ATT_NAME_UPPER#() ;" +NEW_LINE +NEW_LINE +
		TAB + "public void set#ATT_NAME_UPPER#(#TYPE# new#TYPE#);" + NEW_LINE + NEW_LINE;
		
		template = template.replaceAll("#TYPE#", typeName);
		template = template.replaceAll("#ATT_NAME_UPPER#", upperCaseFirstLetter(att.getName()));
		template = template.replaceAll("#MODEL_NAME#", att.getBoDefHandler().getName());
		template = template.replaceAll("#ATT_NAME#", att.getName());
		
		return template;
	}
	
	void createCollectionImplementation(boDefHandler handler) throws IOException {
		
		InputStream in = this.getClass().getResourceAsStream("CollectionTemplate");
		String template = readFile(in);
		
		
		
		String documentation = handler.getLabel();
		template = template.replaceAll("#DESCRIPTION#", documentation);
		template = template.replaceAll("#PACKAGE#", "package "+createPackageDeclaration()+";");
		
		template = template.replaceAll("#TYPE#", handler.getName());
		template = template.replaceAll("#NAME#", handler.getName());
		template = template.replaceAll("#TYPE_LOWER#", handler.getName());
		
		writeToFile(template, handler.getName() + "CollectionImpl.java");
	}
	
	void createCollectionInterface(boDefHandler handler) throws IOException {
		
		InputStream in = this.getClass().getResourceAsStream("CollectionInterfaceTemplate");
		String template = readFile(in);
		
		
		
		String documentation = handler.getLabel();
		template = template.replaceAll("#DESCRIPTION#", documentation);
		template = template.replaceAll("#PACKAGE#", "package "+createPackageDeclaration()+";");
		
		template = template.replaceAll("#TYPE#", handler.getName());
		template = template.replaceAll("#NAME#", handler.getName());
		template = template.replaceAll("#TYPE_LOWER#", handler.getName());
		
		writeToFile(template, handler.getName() + "Collection.java");
	}
	
	private void createCollectionImplementationJSON(boDefHandler handler) throws IOException {
		
		InputStream in = this.getClass().getResourceAsStream("CollectionTemplateJSON");
		String template = readFile(in);
		
		
		
		String documentation = handler.getLabel();
		template = template.replaceAll("#DESCRIPTION#", documentation);
		template = template.replaceAll("#PACKAGE#", "package "+createPackageDeclaration()+";");
		
		template = template.replaceAll("#TYPE#", handler.getName());
		template = template.replaceAll("#NAME#", handler.getName());
		template = template.replaceAll("#TYPE_LOWER#", handler.getName());
		
		writeToFile(template, handler.getName() + "CollectionJSON.java");
	}
	
	/**
	 * 
	 * Generate the class that represents the collection attribute
	 * 
	 * @param bridgeDefinition The bridge definition
	 * @return A string with the object definition
	 * 
	 * @throws IOException
	 */
	private String generateBridge(boDefAttribute bridgeDefinition) throws IOException {
		
		InputStream in = this.getClass().getResourceAsStream("CollectionTemplate");
		String template = readFile(in);
		
		template = template.replaceAll("#ATT_NAME#", bridgeDefinition.getName());
		String documentation = getDocumentation(bridgeDefinition);
		template = template.replaceAll("#DESCRIPTION#", documentation);
		
		template = template.replaceAll("#ATT_NAME_UPPER#", upperCaseFirstLetter(bridgeDefinition.getName()));
		
		String refObjectName = bridgeDefinition.getReferencedObjectName();
		String finalObjectName = "";
		if (refObjectName.equals("boObject"))
			finalObjectName = "BaseModel";
		else
			finalObjectName = refObjectName;
		
		template = template.replaceAll("#TYPE#", finalObjectName);
		template = template.replaceAll("#NAME#", bridgeDefinition.getName());
		template = template.replaceAll("#TYPE_LOWER#", finalObjectName);
		
		String bridgeNameUpperFirstLetter = upperCaseFirstLetter(bridgeDefinition.getName());
		
		String getter = NEW_LINE + TAB + "public " + finalObjectName+"Collection " + "get" + 
			bridgeNameUpperFirstLetter + "(){" + NEW_LINE +
			TAB + TAB + "checkObjectLoaded();" + NEW_LINE;
		getter += NEW_LINE + TAB + TAB + "return new ";
		getter += finalObjectName+"CollectionImpl(object.getBridge("+
			bridgeDefinition.getBoDefHandler().getName()+"Attributes."+bridgeDefinition.getName()+".name()));" + NEW_LINE;
		getter += TAB + " }" + NEW_LINE + NEW_LINE;
		
		template = getter; 
		
		return template;
		
		
	}
	
	
	/**
	 * 
	 * Generate the class that represents the collection attribute
	 * 
	 * @param bridgeDefinition The bridge definition
	 * @return A string with the object definition
	 * 
	 * @throws IOException
	 */
	private String generateBridgeJSON(boDefAttribute bridgeDefinition) throws IOException {
		
		InputStream in = this.getClass().getResourceAsStream("CollectionTemplateJSON");
		String template = readFile(in);
		
		template = template.replaceAll("#ATT_NAME#", bridgeDefinition.getName());
		String documentation = getDocumentation(bridgeDefinition);
		template = template.replaceAll("#DESCRIPTION#", documentation);
		
		template = template.replaceAll("#ATT_NAME_UPPER#", upperCaseFirstLetter(bridgeDefinition.getName()));
		
		String refObjectName = bridgeDefinition.getReferencedObjectName();
		String finalObjectName = "";
		if (refObjectName.equals("boObject"))
			finalObjectName = "BaseModel";
		else
			finalObjectName = refObjectName;
		
		template = template.replaceAll("#TYPE#", finalObjectName);
		template = template.replaceAll("#NAME#", bridgeDefinition.getName());
		template = template.replaceAll("#TYPE_LOWER#", finalObjectName);
		
		String bridgeNameUpperFirstLetter = upperCaseFirstLetter(bridgeDefinition.getName());
		
		String getter = NEW_LINE + TAB + "public " + finalObjectName+"Collection " + "get" + 
		bridgeNameUpperFirstLetter + "(){" + NEW_LINE ;
		getter += NEW_LINE + TAB + TAB + "return new ";
		getter += finalObjectName+"CollectionJSON(bridges.get("+
			bridgeDefinition.getBoDefHandler().getName()+"Attributes."+bridgeDefinition.getName()+".name()));" + NEW_LINE;
		getter += TAB + " }" + NEW_LINE + NEW_LINE;
		
		template = getter; 
		
		return template;
		
		
	}
	
	private String generateInterfaceBridge(boDefAttribute bridgeDefinition) throws IOException {
		
		String bridgeNameUpperFirstLetter = upperCaseFirstLetter(bridgeDefinition.getName());
		
		String refName = bridgeDefinition.getReferencedObjectName();
		if ("boObject".equalsIgnoreCase(refName))
			refName = "BaseModel";
		
		String getter = NEW_LINE + TAB + "public " + refName+"Collection " + "get" + 
		bridgeNameUpperFirstLetter + "();" + NEW_LINE + NEW_LINE;
		
		return getter;
		
		
	}
	
	private String getDocumentation(boDefAttribute attributeDefinition) {
		if (StringUtils.hasValue(attributeDefinition.getDescription()))
			return attributeDefinition.getDescription();
		else if (StringUtils.hasValue(attributeDefinition.getLabel()))
			return attributeDefinition.getLabel();
		else
			return "";
	}

	/**
	 * 
	 * Generate the import statements
	 * 
	 * @param handler The handler for the Model
	 * 
	 * @return A string with the imports
	 */
	String generateImportStatements(boDefHandler handler, boolean fake){
		StringBuilder imports = new StringBuilder(500);
		
		//imports.append("import "+ createPackageDeclarationForImport()+handler.getName()+"Attributes;").append(NEW_LINE);
		imports.append("import netgest.bo.runtime.boRuntimeException;").append(NEW_LINE);
		
		
		if (hasAttributeOfAttribute(boDefAttribute.ATTRIBUTE_BINARYDATA, handler)){
			imports.append("import netgest.io.FSiFile;").append(NEW_LINE);
			imports.append("import netgest.io.iFile;").append(NEW_LINE);
			imports.append("import java.io.File;").append(NEW_LINE);
		}
		boolean dateHasBeenUsed = false;
		if (hasAttributeOfAttribute(boDefAttribute.ATTRIBUTE_DATE, handler)){
			imports.append("import java.util.Date;").append(NEW_LINE);
			dateHasBeenUsed = true;
		}
		if (hasAttributeOfAttribute(boDefAttribute.ATTRIBUTE_DATETIME, handler) && !dateHasBeenUsed){
			imports.append("import java.util.Date;").append(NEW_LINE);
		}
		
		if (hasAttributeOfAttribute(boDefAttribute.ATTRIBUTE_OBJECT, handler)){
			for (boDefAttribute attribute : handler.getAttributesDef()){
				if  (attribute.getType().contains("boObject")){
					imports.append("import com.corrspt.xeo.api.core.XEOObject;").append(NEW_LINE);
					break;
				}
			}
			
		}
		
		if (hasAttributeOfAttribute(boDefAttribute.ATTRIBUTE_OBJECTCOLLECTION, handler)){
			for (boDefAttribute attribute : handler.getAttributesDef()){
				if  (attribute.getType().contains("boObject")){
					imports.append("import com.corrspt.xeo.api.core.BaseModelCollection;").append(NEW_LINE);
					break;
				}
			}
			
		}
		
		if (fake){
			imports.append("import com.corrspt.xeo.api.core.BaseModelCollectionJSON;").append(NEW_LINE);
			imports.append("import com.corrspt.xeo.tests.FakeIFile;").append(NEW_LINE);
		}
		
		return imports.toString();
	}
	
	public boolean hasAttributeOfAttribute(String typeAtt, boDefHandler handler){
		for (boDefAttribute att : handler.getAttributesDef()){
			if (typeAtt.equalsIgnoreCase(att.getAtributeDeclaredType())){
				if (!isSystemAttribute(att.getName()))
					return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * Generate the enumeration
	 * 
	 * @param handler
	 * @return
	 * @throws IOException
	 */
	private String createModelAttsEnum(boDefHandler handler) throws IOException {
		InputStream in = this.getClass().getResourceAsStream("AttEnumTemplate");
		String template = readFile(in);
		
		template = template.replaceAll("#MODEL_NAME#", handler.getName());
		template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
		
		StringBuilder b = new StringBuilder();
		
		boDefAttribute[] attributes = handler.getAttributesDef();
		String toAppend = "";
		String newLine = "\n";
		for ( int attIdx = 0; attIdx < attributes.length ; attIdx++){
			boDefAttribute currAtt = attributes[attIdx];
			if (!isSystemAttribute(currAtt.getName())){
				b.append(toAppend);
				b.append(currAtt.getName());
				b.append("(");
					b.append("\"");
						b.append(cleanNewLines( currAtt.getLabel() ) );
					b.append("\"");
				b.append(",");
				b.append("\"");
				if (currAtt.getDescription() == null)
					b.append("");
				else
					b.append(cleanNewLines( currAtt.getDescription()));
				b.append("\"");
				b.append(")");
				b.append(newLine);
				toAppend = ",";
			}	
		}
		b.append(";");
		template =  template.replaceAll("#ATTS#", b.toString());
		
		return template;
		//writeToFile(template, handler.getName()+"Atts" + ".java");
	}
	
	protected String cleanNewLines(String str){
		str = str.replaceAll("\\\\r","");
		str = str.replaceAll("\\\\n","");
		str = str.replaceAll("\\r","");
		str = str.replaceAll("\\n","");
		return str;
	}
	
	/**
	 * 
	 * Creates the Manager for a given model
	 * 
	 * @param modelHandler Definition of the Model
	 */
	void createModelManager(boDefHandler handler) throws IOException {
		InputStream in = this.getClass().getResourceAsStream("ManagerTemplate");
		String template = readFile( in );
		
		template = template.replaceAll("#IMPORTS#", "import " + createPackageDeclarationForImport() + handler.getName()+";");
		template = template.replaceAll("#MODEL_NAME#", handler.getName());
		template = template.replaceAll("#PACKAGE#", "package " + createPackageDeclaration() + ";");
		template = template.replaceAll("#MODEL_LABEL#", handler.getLabel());
		
		writeToFile(template, handler.getName()+"Manager" + ".java");
		
	}

	
	
	
	
	public static void main(String[] args){
		
		String bodefDeployment = args[0];
		String targetForClasses = args[1]; 
		String packageOfModels = args[2];
		String absolutePathToBase = args[3];
		boolean useInheritance = Boolean.valueOf(args[4]);
		
		//String bodefDeployment = ".build/bodef-deployment";
		//String absolutePathToBase = "/Users/useruser/Documents/git-workspace/OftalPad";
		//String targetForClasses = "teste";
		//String packageOfModels = "com.corrspt.testeJSON";
		//boolean useInheritance = false;
		
		File origin = new File(absolutePathToBase + File.separator + bodefDeployment);
		File destiny = new File(absolutePathToBase + File.separator + targetForClasses);
		ModelGenerator g = new ModelGenerator( origin , destiny, packageOfModels, useInheritance );
		try {
			g.generateAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
