#PACKAGE#

import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boObject;
import com.corrspt.xeo.api.core.BaseModel;
import com.corrspt.xeo.api.core.BOUI;
#IMPORTS#

/**
*  
*   #MODEL_LABEL# Factory
*
* 
*/
public interface #MODEL_NAME#Factory {

	
	
	/**
	 * 
	 * Loads a new instance of #MODEL_NAME#
	 * 
	 * @param ctx The context to load the instance in
	 * @param bouiToLoad The boui of the #MODEL_NAME# to load
	 *  
	 * @return The instance of #MODEL_NAME# 
	 * 
	 */
	public #MODEL_NAME# load(EboContext ctx, long bouiToLoad);
	
	/**
	 * 
	 * Loads a new instance of #MODEL_NAME# in the default context
	 * 
	 * @param bouiToLoad The boui of the #MODEL_NAME# to load
	 *  
	 * @return The instance of #MODEL_NAME# 
	 * 
	 */
	public #MODEL_NAME# load(long bouiToLoad);
	
	
	/**
	 * 
	 * Loads a new instance of #MODEL_NAME# in the default context
	 * 
	 * @param bouiToLoad The identifier of the #MODEL_NAME# to load
	 *  
	 * @return The instance of #MODEL_NAME# 
	 * 
	 */
	public #MODEL_NAME# load(BOUI bouiToLoad);
	
	/**
	*
	* Create a new instance of #MODEL_NAME# in a specific context
	*
	* @param ctx The context to create the instance in
	*
	*/
	public #MODEL_NAME# create(EboContext ctx);
	
	/**
	*
	* Create a new instance of #MODEL_NAME# in the default context
	*
	*/
	public #MODEL_NAME# create();
	
	
	/**
	*
	* Creates a new instance of #MODEL_NAME# with a given parent in a specific context
	* @param ctx The context for the query and object
	* @param parent The parent instance for the new instance
	*
	*/
	public #MODEL_NAME# createWithParent(EboContext ctx, BaseModel parent);
	
	/**
	*
	* Creates a new instance of #MODEL_NAME# with a given parent in default context
	* @param parent The parent instance for the new instance
	*
	*/
	public  #MODEL_NAME# createWithParent(BaseModel parent);
	
	/**
	*
	* Wraps a boObject in an Instance of #MODEL_NAME#
	*
	*	@param instance The instance to wrap as a #MODEL_NAME#	
	*
	*/
	public  #MODEL_NAME# wrapIn#MODEL_NAME#(boObject instance);
	
	/**
	*
	* Wraps a BaseModel in an Instance of #MODEL_NAME#
	*
	*  @param instance The instance to wrap as a #MODEL_NAME#
	*
	*
	*/
	public  #MODEL_NAME# wrapIn#MODEL_NAME#(BaseModel instance);
	
	/**
	*  
	*  Checks if there are instances of #MODEL_NAME#
	* @param ctx The context for the query
	*
	*/
	public  boolean exist#MODEL_NAME#(EboContext ctx);
	
	/**
	*  
	*  Checks if there are instances of #MODEL_NAME#
	* 
	*
	*/
	public  boolean exist#MODEL_NAME#();
	
	/**
	* Checks if there's an instance that matches a set of conditions
	*/
	public boolean existsWhere(String where, Object... args);
	
	/**
	* Loads a given instance matching a set of conditions
	*
	* Returns null if not instance exists
	* Returns the first instance if more than one instance exists
	*/
	public #MODEL_NAME# loadWhere(String where, Object... args);

}