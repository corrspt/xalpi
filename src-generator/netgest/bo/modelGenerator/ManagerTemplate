#PACKAGE#

import java.util.Iterator;
import netgest.bo.runtime.EboContext;
import netgest.bo.runtime.boObject;
import netgest.bo.runtime.boRuntimeException;
import netgest.bo.runtime.boObjectList;
import netgest.bo.runtime.boObjectListBuilder;
import netgest.bo.system.boApplication;
import com.corrspt.xeo.api.core.AbstractModelWrapper;
import com.corrspt.xeo.api.core.BaseModel;
import com.corrspt.xeo.api.core.XeoObjectList;
import com.corrspt.xeo.framework.ObjectList;
import com.corrspt.xeo.framework.XEO;
#IMPORTS#

/**
*  
*   #MODEL_LABEL# 
*
* 
*/
public class #MODEL_NAME#Manager {

	/**
	 * Model name to create new Instances
	 */
	public static final String MODEL_NAME = "#MODEL_NAME#";
	/**
	 * Label of the model
	 */
	public static final String MODEL_LABEL = "#MODEL_LABEL#";
	
	private static #MODEL_NAME#Factory factory = new #MODEL_NAME#FactoryImpl();
	
	public static void setFactory(#MODEL_NAME#Factory newFactory){
		factory = newFactory;
	}
	
	/**
	*
	* Retrieves the current context
	*
	*/
	static EboContext getEboContext(){
		return boApplication.currentContext().getEboContext();
	}
	
	/**
	 * 
	 * Loads a new instance of #MODEL_NAME#
	 * 
	 * @param ctx The context to load the instance in
	 * @param bouiToLoad The boui of the #MODEL_NAME# to load
	 *  
	 * @return The instance of #MODEL_NAME# 
	 * 
	 */
	public static #MODEL_NAME# load(EboContext ctx, long bouiToLoad) {
		return factory.load(ctx,bouiToLoad);
	}
	
	
	/**
	 * 
	 * Loads a new instance of #MODEL_NAME# in the default context
	 * 
	 * @param bouiToLoad The boui of the #MODEL_NAME# to load
	 *  
	 * @return The instance of #MODEL_NAME# 
	 * 
	 * @throws boRuntimeException
	 */
	public static #MODEL_NAME# load(long bouiToLoad) {
		return factory.load(bouiToLoad);
	}
	
	/**
	*
	* Create a new instance of #MODEL_NAME# in a specific context
	*
	* @param ctx The context to create the instance in
	*
	*/
	public static #MODEL_NAME# create(EboContext ctx) {
		return factory.create(ctx);
	}
	
	/**
	*
	* Create a new instance of #MODEL_NAME# in the default context
	*
	*/
	public static #MODEL_NAME# create() {
		return factory.create();
	}
	
	
	/**
	*
	* Creates a new instance of #MODEL_NAME# with a given parent in a specific context
	* @param ctx The context for the query and object
	* @param parent The parent instance for the new instance
	*
	*/
	public static #MODEL_NAME# createWithParent(EboContext ctx, BaseModel parent)  {
		return factory.createWithParent(ctx,parent);
	}
	
	/**
	*
	* Creates a new instance of #MODEL_NAME# with a given parent in default context
	* @param parent The parent instance for the new instance
	*
	*/
	public static #MODEL_NAME# createWithParent(BaseModel parent)  {
		return factory.createWithParent(parent);
	}
	
	/**
	*
	* Wraps a boObject in an Instance of #MODEL_NAME#
	*
	*	@param instance The instance to wrap as a #MODEL_NAME#	
	*
	*/
	public static #MODEL_NAME# wrapIn#MODEL_NAME#(boObject instance){
		
		assert instance != null : "instance cannot be null";
		assert instance.getName().equalsIgnoreCase(MODEL_NAME) : "instance must be of correct type";
		
		return factory.wrapIn#MODEL_NAME#(instance);
	}
	
	/**
	*
	* Wraps a BaseModel in an Instance of #MODEL_NAME#
	*
	*  @param instance The instance to wrap as a #MODEL_NAME#
	*
	*
	*/
	public static #MODEL_NAME# wrapIn#MODEL_NAME#(BaseModel instance){
		
		assert instance != null : "instance cannot be null";
		assert instance.getOriginal().getName().equalsIgnoreCase(MODEL_NAME) : "instance must be of correct type";
		
		return factory.wrapIn#MODEL_NAME#(instance.getOriginal());
	}
	
	/**
	*  
	*  Checks if there are instances of #MODEL_NAME#
	* @param ctx The context for the query
	*
	*/
	public static boolean exist#MODEL_NAME#(EboContext ctx){
		return factory.exist#MODEL_NAME#(ctx);
	}
	
	/**
	*  
	*  Checks if there are instances of #MODEL_NAME#
	* 
	*
	*/
	public static boolean exist#MODEL_NAME#() {
		return factory.exist#MODEL_NAME#();
	}
	
	/**
	*
	*  Checks if there are instances of #MODEL_NAME# that match the conditions
	*
	*  @param conditions The conditions to check (the where clause of a BOQL query, without the where expression)
	*  for instance if you have 'select #MODEL_NAME# where name = ? and age < ?' the condition parameter would
	*  be 'name = ? and age < ?' 
	*  @param args The arguments for the query	
	*
	*/
	public static boolean existsWhere(String where, Object... args){
		return factory.existsWhere(where,args);
	}
	
	
	/**
	*
	*  Retrieves the instance of #MODEL_NAME# that matches the conditions passsed
	*
	*  @param conditions The conditions to check (the where clause of a BOQL query, without the where expression)
	*  for instance if you have 'select #MODEL_NAME# where name = ? and age < ?' the condition parameter would
	*  be 'name = ? and age < ?' 
	*  @param args The arguments for the query	
	*
	*/
	public static #MODEL_NAME# loadWhere(String where, Object... args) throws boRuntimeException{
		return factory.loadWhere(where,args);
	} 
	
	

}