/*Enconding=UTF-8*/
package netgest.bo.def;
import netgest.utils.ngtXMLHandler;

public interface boDefViewer 
{
    public String getViewerName();

    public ngtXMLHandler getForm(String xform);

    public boolean HasForm(String xform);

    public ngtXMLHandler[] getForms();
    
    public boDefHandler getBoDefHandler();
    
    public boDefViewerCategory  getCategory( String category );
    
    public String getObjectViewerClass();
    
    public ngtXMLHandler getChildNode( String nodeName );
    
}