package netgest.bo.lovs;

/**
 * 
 * Represents a pair of
 *
 */
public interface LovPair {

	public String getLabel();
	
	public String getValue();
	
}
