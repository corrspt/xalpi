//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.07.13 at 05:56:46 PM BST 
//


package com.corrspt.models.definition;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transactionModeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="transactionModeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Required"/>
 *     &lt;enumeration value="RequiresNew"/>
 *     &lt;enumeration value="Suports"/>
 *     &lt;enumeration value="NotSuported"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "transactionModeEnum")
@XmlEnum
public enum TransactionModeEnum {

    @XmlEnumValue("Required")
    REQUIRED("Required"),
    @XmlEnumValue("RequiresNew")
    REQUIRES_NEW("RequiresNew"),
    @XmlEnumValue("Suports")
    SUPORTS("Suports"),
    @XmlEnumValue("NotSuported")
    NOT_SUPORTED("NotSuported");
    private final String value;

    TransactionModeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionModeEnum fromValue(String v) {
        for (TransactionModeEnum c: TransactionModeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
