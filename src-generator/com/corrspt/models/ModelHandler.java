package com.corrspt.models;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import netgest.bo.def.boDefHandler;
import netgest.bo.def.v2.boDefHandlerImpl;

import com.corrspt.lovs.definition.XeoLov;

public class ModelHandler {
	
	private static Map<String,boDefHandler> cache = new HashMap<String, boDefHandler>();
	
	private static String fileDirectory = "";
	
	public static void setFileDirectory(String path){
		if (!path.endsWith(File.separator))
			path += File.separator;
		fileDirectory = path;
		boDefHandlerImpl.setFileDirectory(path);
	}

	public static boDefHandler getBoDefinition(String modelName){
		
		if (cache.containsKey(modelName))
			return cache.get(modelName);
		
		System.out.println("Retrieving - " + modelName) ;
		boDefHandler newHandler = boDefHandlerImpl.getBoDefinition(modelName);
		if (newHandler != null){
			cache.put(modelName, newHandler);
			return newHandler;
		} else
			throw new IllegalArgumentException(String.format( "Modelname %s does not exist", modelName ) );
		
	}
	
	private static JAXBContext createJAXBContext() {
		try {
			String packageName = "com.corrspt.lovs.definition";
			JAXBContext jc = JAXBContext.newInstance( packageName );
			return jc;
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static JAXBContext context = createJAXBContext();
	
	public static XeoLov getLovDefinition(String lovName){
		try {
			System.out.println(String.format("Reading %s ",lovName));
			if (!lovName.endsWith(".xeolov")){
				lovName += ".xeolov";
			}
			Unmarshaller u = context.createUnmarshaller();
			XeoLov doc = (XeoLov) u.unmarshal( new FileInputStream(fileDirectory + lovName) );
			return doc;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (ClassCastException e){
			throw new RuntimeException(String.format("Cannot read old lov format file for %s" , lovName) );
		}
		return null;
	}

}
